
var moment = require("moment")
var _ = require('lodash')
// var om = require("./orderManager")

var me = module.exports = {
  simVariables: {
    /** backtest sim variables */
    startBTC: 10,
    
    slippageAmount: .7,    //percent slippage
    backtestTotalTime: 0,   //used for displaying elapsed time
    backtestDaysStart: 7,   //xx days ago
    backtestDaysEnd: 0,     //xx days ago
    //optional fixed dates rather than days past
    backtestDateStart:"2017-10-02 00:00:00",//"2017-04-19 00:00:00", //"2017-06-19 00:00:00",
    backtestDateEnd:"2017-10-04 23:59:59",//"2017-08-18 23:59:59", //"2017-07-10 23:59:59",
    backtestBlockLoadSize: 200000,
    vMaxHoldings: 0,
    enableTier1: false,
    enableTier2: true,
    enableTier3: true
  },
  vars: {
    //variables for checkCurrentHoldings
    t1HoldingAge: 2, // in minutes
    t2HoldingAge: 5, 
    minGain: .5,  //min % gain in first txholdingAge mins
    rebuyTime: 90, //in minutes - don't rebuy if we held at loss within xx mins
    //maxTimeSinceMaxGain: 30
    enableBanking: false,    //bank gains weekly
    bankPeriod: 7,          //bank each x days
    bankDate: undefined,    //last date we banked
    bankOnSell: false,      //flag to know to bank after selling holding
    bankBTCAmount: 10,       //amount to bank over
    enableHistoryPercentMod: false,  //reduce percent with history losses
    enableStopLossModifier: true  //reduce stoploss levels with holding time
  },

  omParams: {
  /** sell variables */
    tier1:{
      maxHoldings: 1,
      stopLossLevels: [
        {percent: -100, stopLoss: 2}, //low % value so always set
        {percent: 1.5, stopLoss: 1.9},//final
        // {percent: 2, stopLoss: 1.4}, //final
        // {percent: 2.5, stopLoss: 1.80}, //final
        //{percent: 4, stopLoss: 1},//final
        {percent: 4, stopLoss: 1},
        // {percent: 4.8, stopLoss: 2},
        // {percent:6, stopLoss: 2},
        // {percent:8, stopLoss: 3},
        // {percent:10, stopLoss: 4},
        // {percent:12, stopLoss: 5}
        // {percent: 6.5, stopLoss: 1},
        // {percent: 7.8, stopLoss: 2},
        // {percent:10, stopLoss: 2},
        // {percent: 20, stopLoss: 1},
        // {percent: 25, stopLoss: 5}
      ],
      /**buy variables */
        tprice: 4,  //5
        tbvol: 6.5,
        tslicesAbove: 6,
        calmFactorBelow:.0068,// .0061,
        checkBuySignal: function(result,holding){
          var t1 = me.omParams.tier1
          var signal = false
          if( result.t_price.t >= t1.tprice &&
            // result.t_bvol.t > result.t_svol.t  &&
              result.t_bvol.t >= t1.tbvol &&
              result.dv_lchunk <= t1.calmFactorBelow &&
              me.validateSlices(result.slicesAboveBVolMean , t1.tslicesAbove)  )
              {signal = 1.1}
          return signal
        }
    },
    tier2:{
      stopLossLevels: [
        {percent: -100, stopLoss: .5}, //low % value so always set
        // {percent: 0, stopLoss: 1},
        {percent: 1, stopLoss: .5},
        {percent: 2, stopLoss: 1.4}, //final
        {percent: 2.5, stopLoss: 1.80},
        {percent: 4, stopLoss: 1.5},
        {percent: 4.8, stopLoss: 2},
        {percent: 6.5, stopLoss: 1},
        {percent: 7.8, stopLoss: 2},
        {percent:10, stopLoss: 2},
        {percent: 20, stopLoss: 1},
        {percent: 25, stopLoss: 5}
        // {percent: 3, stopLoss: 1},
        // {percent: 4, stopLoss: .75}
      ],
      /**buy variables */
        tprice: 5,
        tbvol: 9,
        tsvol: 0,
        // tbtrades: 6,
        tslicesAbove: 8,
        // calmFactorBelow: .0075,
        checkBuySignal: (result,holding)=>{
          var t2 = me.omParams.tier2
          var signal = false
          if( result.t_price.t >= t2.tprice 
            && result.t_bvol.t > t2.tbvol 
            && result.t_svol.t < result.t_bvol.t/14.5
            // && result.dv_lchunk <= t2.calmFactorBelow
            && me.validateSlices(result.slicesAboveBVolMean , t2.tslicesAbove) 
          )
            {signal = 2.1}
          return signal
        }
    },
    tier3:{
      stopLossLevels: [
        {percent: -100, stopLoss: .5}, //low % value so always set
        // {percent: 0, stopLoss: 1},
        {percent: 1, stopLoss: .5},
        {percent: 2, stopLoss: 1.4}, //final
        {percent: 2.5, stopLoss: 1.80},
        {percent: 4, stopLoss: 1.5},
        {percent: 4.8, stopLoss: 2},
        {percent: 6.5, stopLoss: 1},
        {percent: 7.8, stopLoss: 2},
        {percent:10, stopLoss: 2},
        {percent: 20, stopLoss: 1},
        {percent: 25, stopLoss: 5}
        // {percent: 3, stopLoss: 1},
        // {percent: 4, stopLoss: .75}
      ],
      /**buy variables */
        tprice: 7,
        tbvol: 5,
        tsvol: .1,
        tbtrades: 12,
        tslicesAbove: 4,
        //calmFactorBelow: .0068,
        checkBuySignal: (result,holding)=>{
          var t3 = me.omParams.tier3
          var signal = false
          if( result.t_price.t >= t3.tprice 
            && result.t_bvol.t > t3.tbvol 
            && result.t_svol.t < t3.tsvol
            && result.t_btrades.t > t3.tbtrades
            //&& result.dv_lchunk <= t3.calmFactorBelow
            && me.validateSlices(result.slicesAboveBVolMean , t3.tslicesAbove) 
          )
            {signal = 3.1}
          return signal
        }
    },
      /** exclude coins */
      excludeIf: [
        {value:"BTC_",present: false},
        // {value:"XMR", present: true}
      ],
      getSLFromLastGainDate: function(holding,date, sLL){
        if(!me.vars.enableStopLossModifier){return sLL}
        var timeSinceGain = me.getTimeSinceMaxValueInMinutes(holding,date)
        //starting from 0 minutes, start cutting the level
        //by 99% in 60 minutes
        var startAfter = 110
        var maxMinutes = 12
        if(timeSinceGain >= startAfter){
          timeSinceGain-=startAfter
          var ppm = 100/maxMinutes
          var percent = (timeSinceGain * ppm)
          percent = Math.min(99,percent)

          sLL = functions.subtractPercent(sLL,percent)
        }
        return sLL
      },
      //return the sum gain/loss % of the last 10 transactions
      //if we're losing, this would be an indicator to lower
      //the amount of BTC we are trading each trade
      txHistoryPercentModifier: function(tx){
        if(!me.vars.enableHistoryPercentMod){return 1}
        var numTx = 10
        var minBTC = .1
        var le = tx.length 
        if(numTx > le){numTx = le}
        var percentPer = 100/numTx
        var sum = .1
        if(le > 0 && le >=numTx){
          sum = 0
          // var txWeight = 1/numTx
          // var weight = 0
          for(var i = le - numTx;i < le; i++){
            // weight += txWeight
            sum += tx[i].gainLoss //(tx[i].gainLoss * txWeight )
          }
          
          if(sum < 1){
            var maxLoss = 100/percentPer
            var pcLoss = Math.min(Math.abs(sum),maxLoss)

            sum = Math.max(1-( (pcLoss * percentPer)/100),minBTC)
            if(sum < minBTC){
              sum = minBTC
            }
          
          }else{
            sum = 1
          }
          
        }
        
        return sum
      }
  },
  checkTierBuySignal: function(result,holding){
    var tier = 0
    if(me.simVariables.enableTier1){
      tier = me.omParams.tier1.checkBuySignal(result,holding)
    }
    if(!tier && me.simVariables.enableTier2){
      tier = me.omParams.tier2.checkBuySignal(result,holding)
    }
    if(!tier && me.simVariables.enableTier3){
      tier = me.omParams.tier3.checkBuySignal(result,holding)
    }
    
    return tier
  },
  
  
  validateSlices: function(result, required){
    /** 
     * 5: 31,95,159,287,543,607,671,799,863,927
     * 6: 63, 191,319,447, 575,703,831,   
     * 7: 127, 383, 639, 895
     * 8: 255, 767, 
     * 9: 511, 
     * 10: 1023
    */
    
    var s = [1,3,7,15,31,63,127,255,511,1023]

    for(i=9; i >=0; i--){
      if( (s[i] & result) == s[i]){
        if(i+1 >= required){
          return i+1
        }else{
          return false
        }
      }
    }
    return false
     
    //  s[0] = [31,95,159,287,543,607,671,799,863,927]  //5
    //  s[1] = [63,191,319,447,575,703,831]             //6
    //  s[2] = [127,383,639,895]                        //7
    //  s[3] = [255,767]                                //8
    //  s[4] = [511]                                    //9
    //  s[5] = [1023]                                   //10

    // for(i=0;i<=5; i++){
    //   if (_.includes(s[i],result)){
    //     if(i + 5 >= required){
    //       return i + 5
    //     }
    //   }
    // }
    // return false
    // if (_.includes([63,127,255,511, 1023],result)){
    //   if(result >= required){
    //     return true
    //   }
    // }

  },

  checkDisallowedCoins: function(data){
    var rejected = false
    me.omParams.excludeIf.forEach(function(item){
      if(data.pair.includes(item.value) == item.present){
        rejected =  true
      }
    })
    return rejected
  },
  
  getHoldingForDump: function(managedHoldings,endDate){
    //TODO look for lowest performing holding, rather than
    //taking the first one
    // returns a holding object for the caller to dump

    //check tier2 holdings
    for(var holding of managedHoldings){
      if(holding.buyTier >= 2 && holding.type == 0){
        //if we had it for longer than 20 minutes, and it's in the negative, dump it
        if(me.getTimeSinceMaxValueInMinutes(holding,endDate) > me.vars.t2HoldingAge &&
          holding.currentGainPercent < me.vars.minGain){
            //dumpCoin(holding)
            return holding
        }
      }
    }
    //check tier1 holdings
    for(var holding of managedHoldings){
      if(holding.buyTier < 2){
        //if we had it for longer than 20 minutes, and it's in the negative, dump it
        if(me.getTimeSinceMaxValueInMinutes(holding,endDate) > me.vars.t1HoldingAge &&
          holding.currentGainPercent < me.vars.minGain){
            //dumpCoin(holding)
            return holding
        }
      }
    }
    return undefined
  },
  checkHoldingsHistoryForRebuy: function(holdingsHistory, data){
    //returns true if we should not buy coin
    var minAge = me.vars.rebuyTime + 1
    try{
      for(var holding of holdingsHistory){
        if(holding.pair == data.pair){
          if(holding.currentGainPercent < 0){
            minAge = Math.min(me.getTimeSinceMaxValueInMinutes(holding, data.endDate), minAge)
          }
        }
      }
    }catch(e){
      e=e
    }
    if( minAge < me.vars.rebuyTime ){
      return true
    }
  },
  getTimeSinceMaxValueInMinutes: function(holding, sDate){
    if(!sDate){sDate = moment().toDate()}
    age = moment(sDate).diff(holding.buyDate)/1000/60
    return age
  }
  
} // end module exports

Number.prototype.between = function(a, b) {
  var min = Math.min.apply(Math, [a, b]),
    max = Math.max.apply(Math, [a, b]);
  return this > min && this < max;
};
