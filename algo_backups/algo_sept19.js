
var moment = require("moment")
var _ = require('lodash')
// var om = require("./orderManager")

var me = module.exports = {
  simVariables: {
    /** backtest sim variables */
    slippageAmount: .85,  //percent slippage
    backtestTotalTime: 0,  //used for displaying elapsed time
    backtestDaysStart: 7,  //xx days ago
    backtestDaysEnd: 0,  //xx days ago
    //optional fixed dates rather than days past
    backtestDateStart:"2017-09-18 00:00:00",//"2017-04-019 00:00:00", //"2017-06-19 00:00:00",
    backtestDateEnd:"2017-09-20 23:59:59",//"2017-07-18 23:59:59", //"2017-07-10 23:59:59",
    backtestBlockLoadSize: 200000,
    vMaxHoldings: 1
  },
  vars: {
    //variables for checkCurrentHoldings
    t1HoldingAge: 2, // in minutes
    t2HoldingAge: 5, 
    minGain: .5,  //min % gain in first txholdingAge mins
    rebuyTime: 90, //in minutes - don't rebuy if we held at loss within xx mins
    //maxTimeSinceMaxGain: 30
  },

  omParams: {
  /** sell variables */
    tier1:{
      maxHoldings: 1,
      stopLossLevels: [
        {percent: -100, stopLoss: 2.45}, //low % value so always set
        {percent: 1.5, stopLoss: 1.75},//final
        {percent: 2, stopLoss: 1.4}, //final
        {percent: 2.5, stopLoss: 1.80}, //final
        //{percent: 4, stopLoss: 1},//final
        {percent: 4, stopLoss: 1.5},
        {percent: 4.8, stopLoss: 2},
        // {percent:6, stopLoss: 2},
        // {percent:8, stopLoss: 3},
        // {percent:10, stopLoss: 4},
        // {percent:12, stopLoss: 5}
        {percent: 6.5, stopLoss: 1},
        {percent: 7.8, stopLoss: 2},
        {percent:10, stopLoss: 2},
        {percent: 20, stopLoss: 1}
      ],
      /**buy variables */
        tprice: 5,  //5
        tbvol: 7,
        tslicesAbove: 6,
        calmFactorBelow:.0061,// .0061,
        checkBuySignal: function(result){
          var t1 = me.omParams.tier1
          var signal = false
          if( result.t_price.t >= t1.tprice &&
            // result.t_bvol.t > result.t_svol.t  &&
              result.t_bvol.t >= t1.tbvol &&
              result.dv_lchunk <= t1.calmFactorBelow &&
              me.validateSlices(result.slicesAboveBVolMean , t1.tslicesAbove)  )
              {signal = 1.1}
          return signal
        }
    },
    tier2:{
      stopLossLevels: [
        {percent: -100, stopLoss: .25}, //low % value so always set
        // {percent: 0, stopLoss: 1},
        {percent: 1, stopLoss: .5},
        {percent: 2, stopLoss: 1.4}, //final
        {percent: 2.5, stopLoss: 1.80},
        {percent: 4, stopLoss: 1.5},
        {percent: 4.8, stopLoss: 2},
        {percent: 6.5, stopLoss: 1},
        {percent: 7.8, stopLoss: 2},
        {percent:10, stopLoss: 2},
        {percent: 20, stopLoss: 1}
        // {percent: 3, stopLoss: 1},
        // {percent: 4, stopLoss: .75}
      ],
      /**buy variables */
        tprice: 5,
        tbvol: 9,
        tsvol: 0,
        tbtrades: 6,
        tslicesAbove: 8,
        calmFactorBelow: .0075,
        checkBuySignal: (result)=>{
          var t2 = me.omParams.tier2
          var signal = false
          if( result.t_price.t >= t2.tprice 
            && result.t_bvol.t > t2.tbvol 
            && result.t_svol.t < result.t_bvol.t/14
            && me.validateSlices(result.slicesAboveBVolMean , t2.tslicesAbove) 
          )
            {signal = 2.1}
          // if( me.validateSlices(result.slicesAboveBVolMean,10) //|| result.slicesAboveBVolMean == 511
          //   && result.t_svol.t < result.t_bvol.t/2.5
          //   )
          //   {signal = 2.2}
          return signal
        }
    },
      /** exclude coins */
      excludeIf: [
        {value:"BTC_",present: false},
        // {value:"XMR", present: true}
      ]
  },
  checkTierBuySignal: function(result){
    var tier = 0
    if(me.omParams.tier1.checkBuySignal(result)){
      tier = me.omParams.tier1.checkBuySignal(result)
    }
    else 
    if(me.omParams.tier2.checkBuySignal(result)){
      tier = me.omParams.tier2.checkBuySignal(result)
    }
    return tier
  },
  // checkTier1BuySignal: function(result){
  //   var t1 = me.omParams.tier1
  //   var signal = false
  //   if( result.t_price.t >= t1.tprice &&
  //     // result.t_bvol.t > result.t_svol.t  &&
  //       result.t_bvol.t >= t1.tbvol &&
  //       result.dv_lchunk <= t1.calmFactorBelow &&
  //       me.validateSlices(result.slicesAboveBVolMean , t1.tslicesAbove)  )
  //       {signal = 1.1}
  //   return signal
  // },
  // checkTier2BuySignal: function(result){
  //   var t2 = me.omParams.tier2
  //   var signal = false
  //   // if( result.t_price.t >= t2.tprice 
  //   //     && result.t_bvol.t >= t2.tbvol 
  //   //     //&& result.t_btrades.t >= t2.tbtrades
  //   //     && result.t_svol.t <= t2.tsvol 
  //   //     && me.validateSlices(result.slicesAboveBVolMean , t2.tslicesAbove) 
  //   //   )
  //   // {signal = 2.1}
  //   if( result.t_price.t >= t2.tprice 
  //     && result.t_bvol.t > t2.tbvol 
  //     && result.t_svol.t < result.t_bvol.t/3 
  //     && me.validateSlices(result.slicesAboveBVolMean , t2.tslicesAbove) 
  //   )
  //     {signal = 2.1}
  //   if( result.slicesAboveBVolMean == 1023 //|| result.slicesAboveBVolMean == 511
  //       && result.t_svol.t <= t2.tsvol
  //     )
  //     {signal = 2.2}
  //   // if(

  //   // )
  //   //   {signal = 2.3}
  //   return signal
  // },
  validateSlices: function(result, required){
    /** 
     * 5: 31,95,159,287,543,607,671,799,863,927
     * 6: 63, 191,319,447, 575,703,831,   
     * 7: 127, 383, 639, 895
     * 8: 255, 767, 
     * 9: 511, 
     * 10: 1023
    */
    
    var s = [1,3,7,15,31,63,127,255,511,1023]

    for(i=9; i >=0; i--){
      if( (s[i] & result) == s[i]){
        if(i+1 >= required){
          return i+1
        }else{
          return false
        }
      }
    }
    return false
     
    //  s[0] = [31,95,159,287,543,607,671,799,863,927]  //5
    //  s[1] = [63,191,319,447,575,703,831]             //6
    //  s[2] = [127,383,639,895]                        //7
    //  s[3] = [255,767]                                //8
    //  s[4] = [511]                                    //9
    //  s[5] = [1023]                                   //10

    // for(i=0;i<=5; i++){
    //   if (_.includes(s[i],result)){
    //     if(i + 5 >= required){
    //       return i + 5
    //     }
    //   }
    // }
    // return false
    // if (_.includes([63,127,255,511, 1023],result)){
    //   if(result >= required){
    //     return true
    //   }
    // }

  },

  checkDisallowedCoins: function(data){
    var rejected = false
    me.omParams.excludeIf.forEach(function(item){
      if(data.pair.includes(item.value) == item.present){
        rejected =  true
      }
    })
    return rejected
  },
  
  getHoldingForDump: function(managedHoldings,endDate){
    //TODO look for lowest performing holding, rather than
    //taking the first one
    // returns a holding object for the caller to dump

    //check tier2 holdings
    for(var holding of managedHoldings){
      if(holding.buyTier >= 2){
        //if we had it for longer than 20 minutes, and it's in the negative, dump it
        if(me.getTimeSinceMaxValueInMinutes(holding,endDate) > me.vars.t2HoldingAge &&
          holding.currentGainPercent < me.vars.minGain){
            //dumpCoin(holding)
            return holding
        }
      }
    }
    //check tier1 holdings
    for(var holding of managedHoldings){
      if(holding.buyTier < 2){
        //if we had it for longer than 20 minutes, and it's in the negative, dump it
        if(me.getTimeSinceMaxValueInMinutes(holding,endDate) > me.vars.t1HoldingAge &&
          holding.currentGainPercent < me.vars.minGain){
            //dumpCoin(holding)
            return holding
        }
      }
    }
    return undefined
  },
  checkHoldingsHistoryForRebuy: function(holdingsHistory, data){
    //returns true if we should not buy coin
    var minAge = me.vars.rebuyTime + 1
    try{
      for(var holding of holdingsHistory){
        if(holding.pair == data.pair){
          if(holding.currentGainPercent < 0){
            minAge = Math.min(me.getTimeSinceMaxValueInMinutes(holding, data.endDate), minAge)
          }
        }
      }
    }catch(e){
      e=e
    }
    if( minAge < me.vars.rebuyTime ){
      return true
    }
  },
  getTimeSinceMaxValueInMinutes: function(holding, sDate){
    if(!sDate){sDate = moment().toDate()}
    age = moment(sDate).diff(holding.buyDate)/1000/60
    return age
  }
  
} // end module exports

Number.prototype.between = function(a, b) {
  var min = Math.min.apply(Math, [a, b]),
    max = Math.max.apply(Math, [a, b]);
  return this > min && this < max;
};
