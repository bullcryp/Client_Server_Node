
var moment = require("moment")
var _ = require('lodash')
var functions = require('./functions')
// var om = require("./orderManager")

var me = module.exports = {
  simVariables: {
    /** backtest sim variables */
    slippageAmount: .85,  //percent slippage
    backtestTotalTime: 0,  //used for displaying elapsed time
    backtestDaysStart: 7,  //xx days ago
    backtestDaysEnd: 0,  //xx days ago
    //optional fixed dates rather than days past
    backtestDateStart:"2017-04-019 00:00:00",//"2017-08-18 00:00:00",//"2017-04-019 00:00:00", //"2017-06-19 00:00:00",
    backtestDateEnd:"2017-07-18 23:59:59",//"2017-08-25 23:59:59",//"2017-07-18 23:59:59", //"2017-07-10 23:59:59",
    backtestBlockLoadSize: 200000,
    vMaxHoldings: 1
  },
  vars: {
    //variables for checkCurrentHoldings
    t1HoldingAge: 2, // in minutes
    t2HoldingAge: 5, 
    minGain: .5,  //min % gain in first txholdingAge mins
    rebuyTime: 90, //in minutes - don't rebuy if we held at loss within xx mins
    //maxTimeSinceMaxGain: 30
  },

  omParams: {
  /** sell variables */
    tier1:{
      maxHoldings: 1,
      stopLossLevels: [
        {percent: -100, stopLoss: 2.45}, //low % value so always set
        {percent: 1.5, stopLoss: 1.75},//final
        {percent: 2, stopLoss: 1.4}, //final
        {percent: 2.5, stopLoss: 1.80}, //final
        //{percent: 4, stopLoss: 1},//final
        {percent: 4, stopLoss: 1.5},
        {percent: 4.8, stopLoss: 2},
        // {percent:6, stopLoss: 2},
        // {percent:8, stopLoss: 3},
        // {percent:10, stopLoss: 4},
        // {percent:12, stopLoss: 5}
        {percent: 6.5, stopLoss: 1},
        {percent: 7.8, stopLoss: 2},
        {percent:10, stopLoss: 2},
        {percent: 20, stopLoss: 1}
      ],
      /**buy variables */
        tprice: 5,  //5
        tbvol: 7,
        tslicesAbove: 6,
        calmFactorBelow:.0061,// .0061,
        checkBuySignal: function(result){
          var t1 = me.omParams.tier1
          var signal = 0
          if( result.t_price.t >= t1.tprice &&
            // result.t_bvol.t > result.t_svol.t  &&
              result.t_bvol.t >= t1.tbvol &&
              result.dv_lchunk <= t1.calmFactorBelow &&
              me.validateSlices(result.slicesAboveBVolMean , t1.tslicesAbove)  )
              {signal = 1.1}
          return signal
        }
    },
    tier2:{
      stopLossLevels: [
        {percent: -100, stopLoss: .25}, //low % value so always set
        //{percent: 0, stopLoss: 1},
        {percent: 1, stopLoss: .5},
        {percent: 2, stopLoss: 1.4}, //final
        {percent: 2.5, stopLoss: 1.80},
        {percent: 4, stopLoss: 1.5},
        {percent: 4.8, stopLoss: 2},
        {percent: 6.5, stopLoss: 1},
        {percent: 7.8, stopLoss: 2},
        {percent:10, stopLoss: 2},
        {percent: 20, stopLoss: 1}
        // {percent: 3, stopLoss: 1},
        // {percent: 4, stopLoss: .75}
      ],
      /**buy variables */
        tprice: 5,
        tbvol: 9,
        tsvol: 0,
        tbtrades: 6,
        tslicesAbove: 8,
        calmFactorBelow: .0065,
        checkBuySignal: (result,pair)=>{

          var t2 = me.omParams.tier2

          // if( result.t_price.t >= t2.tprice
          //     && result.t_svol.t < result.t_bvol.t/14
          //     && result.dv_lchunk <= t2.calmFactorBelow 
          //     && me.analyseBacktestHistoryBuySignal(pair) > 0)
          //     {return 2.1}

        //   
          var signal = false
          if( result.t_price.t >= t2.tprice 
            && result.t_bvol.t > t2.tbvol 
            && result.t_svol.t < result.t_bvol.t/14
            && me.validateSlices(result.slicesAboveBVolMean , t2.tslicesAbove) 
          )
            {signal = 2.1}
          
          return 0
        }
    },
      /** exclude coins */
      excludeIf: [
        {value:"BTC_",present: false},
        // {value:"XMR", present: true}
      ],
      getSLFromLastGainDate: function(holding,date, sLL){
        var timeSinceGain = me.getTimeSinceMaxValueInMinutes(holding,date)
        //starting from 0 minutes, start cutting the level
        //by 99% in 60 minutes
        var startAfter = 0
        var maxMinutes = 15
        if(timeSinceGain > startAfter){
          timeSinceGain-=startAfter
          var ppm = 100/maxMinutes
          var percent = (timeSinceGain * ppm)
          percent = Math.min(99,percent)

          sLL = functions.subtractPercent(sLL,percent)
        }
        return sLL
      },
  },
  
  checkTierBuySignal: function(result,pair){
    var tier = me.omParams.tier1.checkBuySignal(result)

    if(tier == 0){
      tier = me.omParams.tier2.checkBuySignal(result,pair)
    }

    return tier
  },
  pairBacktestsHistories: [
  /**{pair:"",tbvolHistory:[0,0,0],
              tbtradesHistory:[0,0,0],
              tbpriceHistory:[0,0,0],
              tsvolHistory:[0,0,0]
  } */
  ],
  updateBacktestHistory: function(data){
    var result = JSON.parse(data.results).result
    var t_bv = parseFloat(parseFloat(result.t_bvol.t).toFixed(2))
    var t_bt = parseFloat(parseFloat(result.t_btrades.t).toFixed(2))
    var t_sv = parseFloat(parseFloat(result.t_svol.t).toFixed(2))
    var t_p = parseFloat(parseFloat(result.t_price.t).toFixed(2))
    var pair = me.pairBacktestsHistories.find(x=>x.pair == data.pair)
    if(pair){
      pair.tbvolHistory.unshift(t_bv)
      //pair.tbtradesHistory.unshift(t_bt)
      pair.tbpriceHistory.unshift(t_p)
      pair.tsvolHistory.unshift(t_sv)
      if(pair.tbvolHistory.length > 50){
        pair.tbvolHistory.length = 50
        //pair.tbtradesHistory.length = 50
        pair.tbpriceHistory.length = 50
        pair.tsvolHistory.length = 50
        // for testing - var signal = analyseBacktestHistoryBuySignal(data.pair)
      }
    }else{
      me.pairBacktestsHistories.push({pair:data.pair, tbvolHistory:[t_bv],/**tbtradesHistory:[t_bt],*/tsvolHistory:[t_sv], tbpriceHistory:[t_p]})
    }
  },
  analyseBacktestHistoryBuySignal: function(pair){
    var check = 2.1
    var history = me.pairBacktestsHistories.find(x=>x.pair == pair)
    if(history && history.tbvolHistory.length >= 50){
      /** we should have at least 3 0's behind the final 3 elements */
      for(var i = 3;i <=5;i++){
        if(Math.abs(history.tbvolHistory[i]) >= 1 /**|| history.tbtradesHistory[i] != 0*/ ){
          check = 0
        }
      }
      if(check > 0){
        /** we should have a rising final 3, with the last one being > 4 */
        if( me.checkRisingBacktestHistoryVolume(history.tbvolHistory.slice(0,4))==false){
          check=0
        }else if( me.checkRisingBacktestHistoryVolume(history.tbpriceHistory.slice(0,4))==false ){
          check = 0
        }
        if(check > 0){
          /** sum of array elements 3-20 should be less than ...10? */
          var sum_tbv=0, sum_tbt=0
          for(var i = 3;i <=20;i++){
            sum_tbv += history.tbvolHistory[i]
            //sum_tbt += history.tbtradesHistory[i]
          }
          if( sum_tbv > 10 /**&& sum_tbt > 10*/ ){
            check = 0
          }
        }
      }
      
    }else{
      check = false
    }
    return check
  },
  checkRisingBacktestHistoryVolume: function(arr){
    if(   arr[2] > 1 && 
          arr[1] >= 2 && 
          arr[0] >= arr[1] &&
          arr[0] >= 4 ){
      return true
    }else{
      return false
    }
  },
  // checkTier1BuySignal: function(result){
  //   var t1 = me.omParams.tier1
  //   var signal = false
  //   if( result.t_price.t >= t1.tprice &&
  //     // result.t_bvol.t > result.t_svol.t  &&
  //       result.t_bvol.t >= t1.tbvol &&
  //       result.dv_lchunk <= t1.calmFactorBelow &&
  //       me.validateSlices(result.slicesAboveBVolMean , t1.tslicesAbove)  )
  //       {signal = 1.1}
  //   return signal
  // },
  // checkTier2BuySignal: function(result){
  //   var t2 = me.omParams.tier2
  //   var signal = false
  //   // if( result.t_price.t >= t2.tprice 
  //   //     && result.t_bvol.t >= t2.tbvol 
  //   //     //&& result.t_btrades.t >= t2.tbtrades
  //   //     && result.t_svol.t <= t2.tsvol 
  //   //     && me.validateSlices(result.slicesAboveBVolMean , t2.tslicesAbove) 
  //   //   )
  //   // {signal = 2.1}
  //   if( result.t_price.t >= t2.tprice 
  //     && result.t_bvol.t > t2.tbvol 
  //     && result.t_svol.t < result.t_bvol.t/3 
  //     && me.validateSlices(result.slicesAboveBVolMean , t2.tslicesAbove) 
  //   )
  //     {signal = 2.1}
  //   if( result.slicesAboveBVolMean == 1023 //|| result.slicesAboveBVolMean == 511
  //       && result.t_svol.t <= t2.tsvol
  //     )
  //     {signal = 2.2}
  //   // if(

  //   // )
  //   //   {signal = 2.3}
  //   return signal
  // },
  validateSlices: function(result, required){
    /** 
     * 5: 31,95,159,287,543,607,671,799,863,927
     * 6: 63, 191,319,447, 575,703,831,   
     * 7: 127, 383, 639, 895
     * 8: 255, 767, 
     * 9: 511, 
     * 10: 1023
    */
    
    var s = [1,3,7,15,31,63,127,255,511,1023]

    for(i=9; i >=0; i--){
      if( (s[i] & result) == s[i]){
        if(i+1 >= required){
          return i+1
        }else{
          return false
        }
      }
    }
    return false
     
    //  s[0] = [31,95,159,287,543,607,671,799,863,927]  //5
    //  s[1] = [63,191,319,447,575,703,831]             //6
    //  s[2] = [127,383,639,895]                        //7
    //  s[3] = [255,767]                                //8
    //  s[4] = [511]                                    //9
    //  s[5] = [1023]                                   //10

    // for(i=0;i<=5; i++){
    //   if (_.includes(s[i],result)){
    //     if(i + 5 >= required){
    //       return i + 5
    //     }
    //   }
    // }
    // return false
    // if (_.includes([63,127,255,511, 1023],result)){
    //   if(result >= required){
    //     return true
    //   }
    // }

  },

  checkDisallowedCoins: function(data){
    var rejected = false
    me.omParams.excludeIf.forEach(function(item){
      if(data.pair.includes(item.value) == item.present){
        rejected =  true
      }
    })
    return rejected
  },
  
  getHoldingForDump: function(managedHoldings,endDate){
    //TODO look for lowest performing holding, rather than
    //taking the first one
    // returns a holding object for the caller to dump

    //check tier2 holdings
    for(var holding of managedHoldings){
      if(holding.buyTier >= 2){
        //if we had it for longer than 20 minutes, and it's in the negative, dump it
        if(me.getTimeSinceMaxValueInMinutes(holding,endDate) > me.vars.t2HoldingAge &&
          holding.currentGainPercent < me.vars.minGain){
            //dumpCoin(holding)
            return holding
        }
      }
    }
    //check tier1 holdings
    for(var holding of managedHoldings){
      if(holding.buyTier < 2){
        //if we had it for longer than 20 minutes, and it's in the negative, dump it
        if(me.getTimeSinceMaxValueInMinutes(holding,endDate) > me.vars.t1HoldingAge &&
          holding.currentGainPercent < me.vars.minGain){
            //dumpCoin(holding)
            return holding
        }
      }
    }
    return undefined
  },
  checkHoldingsHistoryForRebuy: function(holdingsHistory, data){
    //returns true if we should not buy coin
    var minAge = me.vars.rebuyTime + 1
    try{
      for(var holding of holdingsHistory){
        if(holding.pair == data.pair){
          if(holding.currentGainPercent < 0){
            minAge = Math.min(me.getTimeSinceMaxValueInMinutes(holding, data.endDate), minAge)
          }
        }
      }
    }catch(e){
      e=e
    }
    if( minAge < me.vars.rebuyTime ){
      return true
    }
  },
  getTimeSinceMaxValueInMinutes: function(holding, sDate){
    if(!sDate){sDate = moment().toDate()}
    age = moment(sDate).diff(holding.lastGainDate)/1000/60
    return age
  }
  
} // end module exports

Number.prototype.between = function(a, b) {
  var min = Math.min.apply(Math, [a, b]),
    max = Math.max.apply(Math, [a, b]);
  return this > min && this < max;
};
