var globals = require("./globals") 
var polon = require("./polo_functions")  
var algo = require('./algo')
var om = require('./orderManager')
var functions = require('./functions')
var db = require("./db/db")

var moment = require("moment")
var _ = require('lodash')
var fs = require('fs')



var me = module.exports = {
  backTestData: undefined,  //store the backtest data so we don't constantly reload it
  simMode: globals.runBacktestOrderManagerSimulation,
  simBestBTCLevel:0, //for the brute force stoplevels loop
  simBestMaxBTC: 0,
  simTmpSLLevel:.49,  //current sim learn SLL amount
  simCurrentSlls: undefined,  //current stop loss levels SET being worked on
  simLoopTmpBestLevel: 0,
  simHighestMaxGain: 0,   // highest BTC reached
  simLearnLoopsCount: 0,  //current number of loops
  simLearnLoops: 3,  //number of loops to complete SLL learning
  simLearnLoopDirectionRev: false,
  simSLStepSize: .25,  //.1 TODO
  simSLLStep: 0, //current sim learn SLL step
  simBVStep: 0,  //current sim learn Buy Variable step
  initLearningLoop: ()=>{
    
        if(!om.initDone){
          om.init(true)
        }
    
        //enable only tier1
        algo.simVariables.enableTier1 = true
        algo.simVariables.enableTier2 = false
        algo.simVariables.enableTier3 = false
        algo.simVariables.enableTier4 = false

        //set the start and end dates
        algo.simVariables.backtestDateStart="2017-10-10 00:00:00"
        algo.simVariables.backtestDateEnd="2017-10-22 23:59:59"

        algo.vars.enableStopLossModifier = false
    
        // set tier - for now. eventually do 2 or more tiers of autotune
        me.simCurrentTier = algo.omParams.tier1 = {
          stopLossLevels: [ {percent: -100, stopLoss: 2} ],
          tprice: 3,
          tbvol: 3,
          tslicesAbove: 6,
          tbvol_tsvol_ratio: .1,
          calmFactorBelow: .0068,
          checkBuySignal: (result,holding)=>{
            var signal = false
            var t1 = algo.omParams.tier1
            if(result.t_price.t >= t1.tprice){
              if(result.t_bvol.t > t1.tbvol){
                if(result.dv_lchunk <= t1.calmFactorBelow){
                  if(result.t_svol.t < result.t_bvol.t/t1.tbvol_tsvol_ratio){
                    if(algo.validateSlices(result.slicesAboveBVolMean, t1.tslicesAbove)){
                      signal = 1.1
                    }
                  }
                }
              }
            }
            return signal
          }
        }
        me.simCurrentTierValue = 1.1
        me.simCurrentSlls = me.simCurrentTier.stopLossLevels = []

        // initialize test SLL set
        setTestStartingSLL(-1,2.5)    // set all levels to 2 to start
        function setTestStartingSLL(p,s){
          //if(noPrev == undefined){noPrev=false}
          setSLLevel(p,s)
        }

        //initialize buy variables
        me.buyvariables = [
          { value: 3, max: 10, step: .1, name: 'tprice'},
          { value: 3, max: 10, step: .1, name: 'tbvol'},
          { value: 3, max: 10, step: .1, name: 'tsvol'},
          { value: 1, max: 20, step: .5, name: 'tbvol_tsvol_ratio'},
          { value: 3, max: 10, step: 1, name: 'tslicesAbove'},
          { value: .002, max: .05, step: .00192, name: 'calmFactorBelow'},
        ]

        //get total rows in backtest table
        db.getBacktestTotalRows(algo.simVariables.backtestDaysStart, algo.simVariables.backtestDaysEnd,
          algo.simVariables.backtestDateStart, algo.simVariables.backtestDateEnd,
          function (res) {
            var totalRows = res
            algo.simVariables.backtestBlockLoadSize = totalRows + 1
            getBackTestData(function () {
              startLoops()
            })
        })

        //do the SLL learning Loops
        
        function startLoops(){
          //loop to train the SL Levels
          doLearningLoops(doSllLearningLoop, function(){

              //reset the loops count
              me.simLearnLoopsCount = 0
              //loop to train the buy Entry Points
              doLearningLoops(doBuyEntryLearningLoop)

          })
          
        }
    
        
            //end loop content
          
  }
}
function doLearningLoops(method, callback){
  var ends
  do {
    do {
      runBackTestLoop()           //run the backtest with current settings levels
      ends = method() //doSllLearningLoop()  //analyse results, adjust SLL, and continue next loop
    } while (!ends)

  } while (me.simLearnLoopsCount < me.simLearnLoops);
  console.log("     *****" + me.simBestSLLSet + "   BTC:" + me.simBestBTCLevel)
  var end = 0

}
function doBuyEntryLearningLoop(){
  try {
    var maxGain = om.managedHoldingsHistory.reduce(function(l,e){
      return e.maxGain > l.maxGain ? e : l
    }).maxGain
  } catch (error) {return false}  //if wee err here, array is empty = no trades
    if(maxGain > me.simHighestMaxGain){
      me.simHighestMaxGain = maxGain
    }
    om.managedHoldingsHistory = []
    om.accountObject.transactions = []
    om.accountObject._BTCAvailable = algo.simVariables.startBTC
    om.accountObject.startBTC = algo.simVariables.startBTC
    om.managedHoldings = []
    // var sl = getSLLevel()
    if(me.simBestBTCLevel < om.accountObject.simResult.endBTC){
      me.simBestBTCLevel = om.accountObject.simResult.endBTC
      me.simTmpSLLevel = functions.round(sl,2)
      me.simBestSLLSet = JSON.stringify(me.simCurrentSlls.filter(x=>x.percent <= me.simSLLStep)).replaceAll("percent","p").replaceAll("stopLoss","sl").replaceAll("prevStopLoss","psl")
      console.log("     *****" + me.simBestSLLSet + "   BTC:" + me.simBestBTCLevel)
    }
    if(me.simBestMaxBTC < om.accountObject.simResult.maxBTCHeld){
      me.simBestMaxBTC = om.accountObject.simResult.maxBTCHeld
    }
    var bl = getBLevel()
    if(me.simBestBTCLevel < om.accountObject.simResult.endBTC){
      me.simBestBTCLevel = om.accountObject.simResult.endBTC
      me.simTmpSLLevel = functions.round(sl,2)
      me.simBestSLLSet = JSON.stringify(me.simCurrentSlls) //.replaceAll("percent","p").replaceAll("stopLoss","sl").replaceAll("prevStopLoss","psl")
      console.log("     *****" + JSON.stringify(JSON.parse(me.simBestSLLSet).filter(x=>x.percent <= me.simSLLStep)).replaceAll("percent","p").replaceAll("stopLoss","sl").replaceAll("prevStopLoss","psl") + "   BTC:" + me.simBestBTCLevel)
      bl = bl
    }



  return false
}
/** Loop Functions for Sim and Backtest */
function doSllLearningLoop(){

  try {
    var maxGain = om.managedHoldingsHistory.reduce(function(l,e){
      return e.maxGain > l.maxGain ? e : l
    }).maxGain
  } catch (error) {return false}  //if wee err here, array is empty = no trades
    if(maxGain > me.simHighestMaxGain){
      me.simHighestMaxGain = maxGain
    }
    om.managedHoldingsHistory = []
    om.accountObject.transactions = []
    om.accountObject._BTCAvailable = algo.simVariables.startBTC
    om.accountObject.startBTC = algo.simVariables.startBTC
    om.managedHoldings = []

      var sl = getSLLevel()
      if(me.simBestBTCLevel < om.accountObject.simResult.endBTC){
        me.simBestBTCLevel = om.accountObject.simResult.endBTC
        me.simTmpSLLevel = functions.round(sl,2)
        me.simBestSLLSet = JSON.stringify(me.simCurrentSlls) //.replaceAll("percent","p").replaceAll("stopLoss","sl").replaceAll("prevStopLoss","psl")
        me.simBestSLLSetFinal = JSON.stringify(me.simCurrentSlls.filter(x=>x.percent <= (me.simSLLStep + .5) ))
        console.log("     *****" + JSON.stringify(JSON.parse(me.simBestSLLSet).filter(x=>x.percent <= me.simSLLStep)).replaceAll("percent","p").replaceAll("stopLoss","sl").replaceAll("prevStopLoss","psl") + "   BTC:" + me.simBestBTCLevel)
        me.simLoopTmpBestLevel = 0
      }else if(me.simBestBTCLevel == om.accountObject.simResult.endBTC && me.simLoopTmpBestLevel == 0){
        me.simLoopTmpBestLevel = sl
      }
      if(me.simBestMaxBTC < om.accountObject.simResult.maxBTCHeld){
        me.simBestMaxBTC = om.accountObject.simResult.maxBTCHeld
      }
      sl += me.simSLStepSize
      if(sl > 5){
        //set current levels to BEST stoploss level
        me.simCurrentSlls = me.simCurrentTier.stopLossLevels = JSON.parse(me.simBestSLLSet) 
        //finalize SL set
        setSLLevel(me.simSLLStep,me.simLoopTmpBestLevel,true)
        //increment half a percent
        me.simSLLStep += .5  //(me.simLearnLoopDirectionRev==false)?.5:-.5
        
        //remove the new default stop from the set until tested
        var index = me.simCurrentSlls.findIndex(x=>x.percent == me.simSLLStep)
        me.simCurrentSlls.splice(index,1)
        me.simBestSLLSet = JSON.stringify(me.simCurrentSlls)

        me.simTmpSLLevel = .49
        me.simLoopTmpBestLevel = 0
        
        
        if(me.simSLLStep <=20 && me.simSLLStep >=0){
          
          if (me.simHighestMaxGain < me.simSLLStep){
            /**we don't need to continue, 
             * no holding had a gain higher
             * than this level */
            exitLoop()
            return true
          }
          //and add the new row
          setSLLevel(me.simSLLStep,.5)
          
        }else{
          // pc over 20, we are done
          exitLoop()
          return true
        }
      }else{
        setSLLevel(me.simSLLStep,sl)
      }
      function exitLoop(){
        console.log("BTC: " + me.simBestBTCLevel)
        console.log("MAX: " + me.simBestMaxBTC)
        me.simSLLStep = 0 //?
        me.simLearnLoopsCount += 1
        //me.simLearnLoopDirectionRev = !me.simLearnLoopDirectionRev
        var tmp = getSLLevel(undefined,true)
      }
      return false
}
function getBLevel(reset){
  var levels = me.buyvariables
  var bl = me.simBVStep
  return {
    value: levels[bl].value,
    max: levels[bl].max,
    step: levels[bl].step
  }
}
function setBLevel(index,bl,final,setPrev){
  bl = functions.round(bl,5)
  var levels = me.buyvariables
  if(levels[index].max >= bl){
    levels[index].value = bl
  }else{
    return false
  }

}
function getSLLevel(percent,reset){
  var levels = me.simCurrentSlls
  var sll= me.simSLLStep
  if(sll == 0){sll = -100}
  var index = levels.findIndex(x=>x.percent == sll)
  if(index >=0){
    if(reset){
      levels[index].stopLoss = .5
    }
    if(percent){
      return levels[index].percent
    }else{
      return levels[index].stopLoss
    }
  }else{
    return 0
  }
  
}
function setSLLevel(percent,sl, final){
  var levels = me.simCurrentSlls
  if(percent==0){percent = -100}
  var index = levels.findIndex(x=>x.percent == percent)
  if(percent == -1){
    //set all levels to a starting value
    for(i=0;i<=20;i+=.5){
      var p = i; if (p==0){p=-100}
      levels.push({percent:p,stopLoss:Math.min(Math.max(p,.5),5)})
    }
    // levels[0].stopLoss = .5
    // levels[40].stopLoss = 3
    me.simBestSLLSet = JSON.stringify(levels)
  }else if(final){
      if(sl > 0){
        if(index < 0){
          levels.push( {percent: percent, stopLoss: functions.round(sl,2)} )
          levels.sort( function(a,b){ return a.percent - b.percent} )
        }else{
          levels[index].stopLoss = functions.round(sl,2)
        }
        
      }
      if(index < 0){
        index = levels.findIndex(x=>x.percent == percent)
      }
      if(index > 0 && levels[index].stopLoss == levels[index-1].stopLoss){
        levels.splice(index,1)
      }
      me.simBestSLLSet = JSON.stringify(me.simCurrentSlls) //.replaceAll("percent","p").replaceAll("stopLoss","sl").replaceAll("prevStopLoss","psl")
      me.simBestSLLSetFinal = JSON.stringify(me.simCurrentSlls.filter(x=>x.percent <= (me.simSLLStep + .5) ))
  }else{
    
    if(index >=0){
      levels[index].stopLoss = functions.round(sl,2)
      /** if(final){
        //   if(sl != .49){
        //     levels[index].prevStopLoss =  functions.round(sl,2)
        //   }else{
        //     if(levels[index].prevStopLoss){
        //       levels[index].stopLoss = levels[index].prevStopLoss
        //     }else{
        //       if(index > 0){
        //         levels.splice(index,1)
        //       }
        //     }
        //   }
        //   try{
        //     if(index > 0 && levels[index].stopLoss == levels[index-1].stopLoss){
        //       levels.splice(index,1)
        //     }
        //   }catch(e){}
        // }
        
        // if( (sl==.49 && index > 0) || (final && index > 0)  ){
        //   if( sl==.49 || (levels[index].stopLoss == levels[index-1].stopLoss) ){
        //     levels.splice(index,1)
        //   }
       }*/
    }else{
      var lvl = {percent: percent, stopLoss: sl}
      levels.push(lvl)
      levels.sort( function(a,b){ return a.percent - b.percent} )
    }
  }
  var log
  log = JSON.stringify(levels.filter(x=>x.percent <= Math.max(percent,0) )).replaceAll("percent","p").replaceAll("stopLoss","sl").replaceAll("prevStopLoss","psl")
  if(final){log += " - F"}
  console.log (log)
}
function runBackTestLoop(){
  
  //console.log("Getting backtest Group")
  //load the rows in groups of 10,000, and send them in sequence to simulate real trades
  //functions.timeEvent(true)
    
  if(me.backTestData.length > 0){
      
      for(item of me.backTestData){
        om.analyseCryptoReport(item,true)
      }  //518,483
      // me.backTestData.forEach(function(item,index){
      //   // if( (index % 10000) == 0){console.log(index)}
      //   om.analyseCryptoReport(item,true)
      // })

      // groupNum += 1
      //algo.simVariables.backtestTotalTime += functions.timeEvent()
        //functions.timeEvent()
        //console.log("Backtest simulation end")
      
        //sell any remaining holdings
        // om.managedHoldings.forEach(function(holding){
        //   om.dumpCoin(holding)
        // })
        for(holding of om.managedHoldings){
          om.dumpCoin(holding)
        }

        var gainloss = 0
        var maxGain = 0
        om.accountObject.transactions.forEach(function(item){
          gainloss += item.gainLoss
          maxGain += item.maxGain
        })

        om.managedHoldingsHistory.sort(function(a,b){ return new Date(a.buyDate) - new Date(b.buyDate)})
        
        om.accountObject.simResult = {
          startBTC: algo.simVariables.startBTC,
          endBTC: om.accountObject.getTotalAccountBTCValue(),
          minBTCHeld: om.accountObject.minBTCHeld,
          maxBTCHeld: om.accountObject.maxBTCHeld,
          maxBTCDate: om.accountObject.maxBTCDate,
          maxholdings: algo.simVariables.vMaxHoldings,
          totalGain: gainloss, 
          totalMaxGain: maxGain,
          variables: algo.omParams,
          simVariables: algo.simVariables,
          transactions: me.managedHoldingsHistory,
          log: om.accountObject.simLog
        }
        
        console.log(
          "endBTC: " + om.accountObject.simResult.endBTC + 
        "   bestEndBTC: " + me.simBestBTCLevel + 
        "   maxBTC: " + om.accountObject.simResult.maxBTCHeld + 
        "   largestMaxGain: " + me.simHighestMaxGain +
        "   Current Loop: " + Math.round(me.simLearnLoopsCount + 1) + " of " + me.simLearnLoops
        )
        
        
        /** TODO - check bitcoin status if going up or down
         * don't buy coins when bitcoin increasing
         */
        
        
        return
        
      
  }
  
}
function getBackTestData(callback){
  var groupNum = 0
  var start = groupNum * algo.simVariables.backtestBlockLoadSize
  var count = algo.simVariables.backtestBlockLoadSize
  
  if(!me.backTestData){
    db.getBacktestRows(start, count,algo.simVariables.backtestDaysStart,algo.simVariables.backtestDaysEnd,
      algo.simVariables.backtestDateStart,algo.simVariables.backtestDateEnd,
      groupNum, function(res,groupNum){
        if(res.length > 0){
          me.backTestData = res
          if(callback){
            callback()
          }
        }
      })
  }else{
    callback()
  }
}
