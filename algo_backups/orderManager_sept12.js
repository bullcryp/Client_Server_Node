
var globals = require("./globals") 
var polon = require("./polo_functions")  
var algo = require('./algo')
var functions = require('./functions')
var moment = require("moment")
var db = require("./db/db")
var _ = require('lodash')
var fs = require('fs')


/** local variables */
var gotOrders = false
var gotHoldings = false

/**  MODULE EXPORTS */
var me = module.exports = {
  managedHoldings: globals.account.managedHoldings,
  managedHoldingsHistory: [],
  // holdings: globals.account.holdings,
  // orders: globals.account.orders,
  initDone: false,
  
  simMode: globals.runBacktestOrderManagerSimulation,
  simBestBTCLevel:0, //for the brute force stoplevels loop
  simBestSLLevel:.1,

  /** called when we got all current orders from poloniex */
  gotOrders: ()=>{
    gotOrders = true
    me.init()
  },
  /** called when we got all current holdings from poloniex */
  gotHoldings: ()=>{
    gotHoldings = true
    me.init()
  },
  isInManagedHoldings: (pair)=>{
    var holding = me.managedHoldings.find(x=>x.pair == pair)
    if(holding != undefined){
      return holding
    }else{
      return undefined
    }
  },
  accountObject: {},
    realAccount: {
      sim: false,
      /** helper function - return amount of BTC available */
      BTCAvailable: function(){ 
          var holding = globals.account.holdings.find(x => x[0]=="BTC")
          if(holding){
            return holding[1].available * .995
          }else{
            return 0
          }
        },
      /** helper function - return amount of USDT available */
      USDTAvailable: function(){
          var holding = globals.account.holdings.find(x => x[0]=="USDT")
          if(holding){
            return holding[1].available
          }else{
            return 0
          }
      },
      /** helper function - return amount of ETH available */
      ETHAvailable: function(){
          var holding = globals.account.holdings.find(x => x[0]=="ETH")
          if(holding){
            return holding[1].available
          }else{
            return 0
          }
      },
      /** helper function - return count of managed orders */
      managedHoldingsCount: function(){return me.managedHoldings.length},
      /** helper function - return total BTC value of account */
      getTotalAccountBTCValue: function(){
        var totalBTC = 0
        globals.account.holdings.forEach(function(coin){
          totalBTC+= parseFloat(coin[1].btcValue) 
        })
        return totalBTC
      },
      transactions: [],
    },
    simAccount: {
      sim: true,
      /** helper function - return amount of BTC available */
      BTCAvailable: function(){return this._BTCAvailable},
      _BTCAvailable: 10,
      USDTAvailable: function(){return this._USDTAvailable},
      _USDTAvailable: 0,
      ETHAvailable: function(){return this._ETHAvailable},
      _ETHAvailable: 0,
      /** helper function - return count of managed orders */
      managedHoldingsCount: function(){
        var count = 0
        me.managedHoldings.forEach(function(item){
          if(item.stage < orderStageEnum.sell){
            count ++
          }
        })
        return count
      },
      getTotalAccountBTCValue: function(){
        var totalBTC = 0
        me.holdings.forEach(function(coin){
          totalBTC+= parseFloat(coin[1].btcValue) 
        })
        totalBTC += this.BTCAvailable()
        return totalBTC
      },
      transactions: [],
      simResult: {},
      minBTCHeld: 10,
      maxBTCHeld: 10,
      maxBTCDate: "",
      simLog: ""
      
    },
  /** each analysis packet comes thru for buy, dump, or hold*/
  analyseCryptoReport: (data)=>{
    //assume for now always buying from BTC

    /** check for coins excluded from trading */
    if(algo.checkDisallowedCoins(data)){
      return
    }

    var result = data.results?JSON.parse(data.results).result:data.result

    /** 1) - do we hold this pair? */
    var holding = me.isInManagedHoldings(data.pair)
    if (holding && holding.stage < orderStageEnum.sell){
      /** we are already holding this coin... decide what to do with the result */
      updateHoldingPrice(holding, result,data.endDate, function(gainPercent, prevGainPercent){
      //correctHoldingAmount(holding,result)
        simLog(holding.pair + ", gain  " + parseFloat(gainPercent).toFixed(2) + " percent, " + parseFloat(gainPercent - (holding.maxGain - holding.stopLossLevel)).toFixed(1) + " above stop loss", data.endDate)
        txLogToFile("gl - " + holding.pair + ", gain  " + parseFloat(gainPercent).toFixed(2) + " percent")

        /** 2) do we want to dump this coin? */
        var dump = false
        var holdingMaxGain = Math.max(holding.maxGain, 0)
        if(gainPercent < (holdingMaxGain - holding.stopLossLevel))
          //triggered stoploss level
          {dump = true}
        else {
          var minsSinceGain = algo.getTimeSinceMaxValueInMinutes(holding, data.endDate)
          if (minsSinceGain >= algo.vars.maxTimeSinceMaxGain){ 
            if(gainPercent < prevGainPercent)
              {dump = true}
          //   if(result.slicesAboveBVolMean < 63 && gainPercent < prevGainPercent)
          //     {dump = true}
          }
        }

        if(dump){
          holding.sellDate = data.endDate || new Date()
          
          dumpCoin(holding, holding.sellDate)
        }

      })
    }else{

      /** 3) do we want to buy this coin?  TODO make this function buy the coin  */
      checkCoinForPurchase(data,result)
    }

  },

  /** Loop functions */
  initBacktestOrderManagerSimulation: (callback)=>{
    //run init() in sim mode
    if(!me.initDone){
      me.init(true)
    }
    //prepare the backtestscopy table

    if(globals.orderManagerSimulationRebuildData){
      console.log("Backtest prepare table")
      db.makeBacktestClone(function(){
         initLoops(callback)
      })
    }else{
      initLoops(callback)
    }
    //
    function initLoops(callback){
      //get total rows in backtest table
      db.getBacktestTotalRows(algo.simVariables.backtestDaysStart,algo.simVariables.backtestDaysEnd,
                              algo.simVariables.backtestDateStart,algo.simVariables.backtestDateEnd,
                              function(res){
        var totalRows = res
        var groups = parseInt(totalRows / algo.simVariables.backtestBlockLoadSize)
        
        functions.timeEvent(true)
        
        runBackTestLoop(0,groups, callback)
      })
    }
      
    //})
  },
  initLearningLoop: ()=>{

    if(!me.initDone){
      me.init(true)
    }

    algo.omParams.tier1.stopLossLevels = []
    var slls = algo.omParams.tier1.stopLossLevels

    slls.push({percent: -100, stopLoss: .5})
    doLearningLoop()

    
        //end loop content
      
  },

  /** Init function.. executed once, after gotOrders and gotHoldings become true */
  init: (sim) =>{
    if(sim != true && (me.initDone || !gotOrders || !gotHoldings)){return}

    if(sim != true && (!globals.orderManagerSimulationMode && !globals.orderManagerEnableLiveTrading)){return}

    if(me.initDone){return}

    me.managedHoldings = globals.account.managedHoldings
    me.holdings = globals.account.holdings

    if(globals.orderManagerSimulationMode){
      me.accountObject = me.simAccount
    }else{
      me.accountObject = me.realAccount
    }
    // TODO for testing forced buy
    setTimeout(function() {
      // createBuySignal("BTC_LBC",.000157)
    }, 120000);
    

    /** we have orders and holdings gotten from polo,  Now load our managed orders 
     * objects which are created independently of what polo returns to us
     */

    // me.loadManagedHoldings()

    /** initialize managed holdings interval */

            /* debug data TODO remove this */
            console.log("USD Availalbe: " + me.accountObject.USDTAvailable())
            console.log("BTC Available: " + me.accountObject.BTCAvailable())
            console.log("ETH Available: " + me.accountObject.ETHAvailable())
            console.log("Managed Holdings Count: " + me.accountObject.managedHoldingsCount())
            console.log("Total BTC Value: " + me.accountObject.getTotalAccountBTCValue())
    
    if(!isSim()){
      orderManagerLoop()
      //TODO remove - debugging
      //me.managedHoldings.push(createManagedHolding("BTC_DGB",new Date(),.00000607,0.0080000112,123,2,1 ))
      // me.managedHoldings.push(createManagedHolding("BTC_DGB",new Date(),.00000607,0.0009985,124,2,1 ))
   
      //load managed orders
      loadManagedHoldings()
    }else{

    }

    txLogToFile("Startup")
    me.initDone = true
  },
}
/**  END MODULE EXPORTS */

/** Live Trading Functions */
function orderManagerLoop(){
  /** order manager interval loop. Runs every 10 seconds
  * to check on managed orders */
  if(!isSim()){
    var omInterval = setInterval(function(){
      maintainHoldings.loop()
    },10000) //every 10 seconds
  }
}
var maintainHoldings = {
  loop: function(){
    if(!isSim()){
      for(var holding of me.managedHoldings){
        //correct holding amount
        // this.verifyHoldingAmount(holding)
        switch (holding.stage) {
          case orderStageEnum.purchase:
            break;
          case orderStageEnum.forcePurchase:
            this.forcePurchase(holding)
            break;
          case orderStageEnum.purchaseComplete:
            this.verifyHoldingAmount(holding)
            break;
          case orderStageEnum.purchaseComplete:
            break;
          case orderStageEnum.sell:
            break;
          case orderStageEnum.dump:
            this.dump(holding)
            break;
          default:
            break;
        }
      }
    }
  },
  purchase: function(holding){
  },
  forcePurchase: function(holding){
    //currencyPair, price, amount, fillOrKill, immediateOrCancel, postOnly, callback
    var price = holding.oPrice * globals.buyNowFactor

    //if too many attemps, stop trying to buy this coin
    if(holding.purchaseAttempts > 4){
      maintainHoldings.moveHoldingToHistory(holding)
      return
    }
    polon.polo_methods.buy(holding,price,0,0,0, function(err,res){
      getHoldingsFromPolo(1)  //refresh the account data from polo, no matter the result
      if(!err){
        correctHoldingAmount(holding,res)
      }else{
        //if err, try again in 5 seconds
        holding.purchaseAttempts +=1
        holding.purchaseAttemptDate = new Date()
        setTimeout( ()=>{maintainHoldings.loop()}, 5000 );  //re-run sooner if error
      }
    })
  },
  sell: function(holding){
  },
  dump: function(holding){
    this.verifyHoldingAmount(holding)
    polon.polo_methods.dumpOrder(holding,0,0,0, function(err,res){
      getHoldingsFromPolo(1)  //refresh the account data from polo, no matter the result
      if(!err){
        //TODO - determine the actual sell price and amount, and update the holding history
        holding.stage = orderStageEnum.sold
        maintainHoldings.moveHoldingToHistory(holding)
      }else{
        setTimeout( ()=>{maintainHoldings.loop()}, 2000 );  //re-run sooner if error
      }
    })
  },
  moveHoldingToHistory: function(holding){
    //add item to history
    me.managedHoldingsHistory.push(holding)
    
    if(!isSim()){ //save to history.. if not in sim mode
      db.saveManagedHoldingsHistory(holding)
    }
    //create a transaction "receipt"
    me.accountObject.transactions.push({
      pair: holding.pair,
      gainLoss: holding.currentGainPercent,
      maxGain: holding.maxGain,
      currentBTC: me.accountObject.BTCAvailable(),
      date: holding.buyDate
    })
    //remove it from managedHoldings
    me.managedHoldings.forEach(function(item,index){
      if (item.pair == holding.pair){
        me.managedHoldings.splice(index,1)
      }
    })
    saveManagedHoldings()
  },
  verifyHoldingAmount: function(holding){
    for(item of globals.account.holdings){
      if(item[0] == holding.coin){
        holding.amount = parseFloat(item[1].available)
        holding.oAmount = holding.amount
        holding.stage = orderStageEnum.purchasedConfirmed
        break;
      }
    }
    saveManagedHoldings()
  }
}
/** End Live Trading Functions */

/** General trading functions */
function updateHoldingPrice(holding, result, date, callback){
  var prevGainPercent, gainPercent
  prevGainPercent = holding.currentGainPercent
  holding.xf_setPrice(result.price)
  gainPercent = holding.currentGainPercent
  if ( gainPercent >  prevGainPercent && gainPercent >= holding.maxGain){ // gain is increasing
    holding.lastGainDate = date
  }
  // if(holding.price > holding.maxPrice){
  //   holding.maxPrice = holding.price
  // }
  //set last trade date..not sure why yet
  holding.lastTrade = date
  
  var maxGainPercent = holding.maxGain
  //check and set stoplosslevel
  holding.stopLossLevels.forEach(function(item){
    if (maxGainPercent >= item.percent){
      holding.stopLossLevel = item.stopLoss
    }
  })
  callback(gainPercent, prevGainPercent)
}
function correctHoldingAmount(holding, res){
  //update managed holding from the polo transaction response
  var amount=0
  var WAPrice = 0
  holding.stage = orderStageEnum.purchaseComplete

  var trades = res.resultingTrades
  
  //TODO - determine the actual buy price and amount, and update the holding
  //{"orderNumber":31226040,"resultingTrades":[{"amount":"338.8732","date":"2014-10-18 23:03:21","rate":"0.00000173","total":"0.00058625","tradeID":"16164","type":"buy"}]}
  if(trades.length > 0){
    for(trade of trades){
      amount += trade.amount
      WAPrice += (trade.amount * trade.rate) //calculate the weighted average price
    }
    WAPrice = WAPrice / amount  //complete the weighted average price
    holding.oPrice = WAPrice  //set weighted average price paid as the orig price
    holding.oAmount =  amount  //set the amount calculated as orig amount
    holding.amount = amount
    holding.xf_setValues()  //recalculate G/L
  }
}

function checkCoinForPurchase(data,result){
    var tier = algo.checkTierBuySignal(result)
    if( tier > 0 ){
        // set some initial vars
        var price = result.price
        var oDate = result.endDate || data.endDate

        simLog("Tier " + tier + " Buy signal on " + data.pair)
        
      //get the number of holdings slots remaining
        var slotsRemaining = algo.omParams.tier1.maxHoldings - me.accountObject.managedHoldingsCount()
      
      // if no slots, see if there are any coins that need dumping
        if(slotsRemaining <=0 ){
          var holding = algo.getHoldingForDump(me.managedHoldings,oDate)
          if(holding){
            dumpCoin(holding, oDate)
          }
          return
        }

      /** check if we held this recently, and if so did we sell at loss. 
         * if so, do not buy within xx time  */
        if(algo.checkHoldingsHistoryForRebuy(me.managedHoldingsHistory, data ) ){return}

        
      //calculate how much BTC available for purchases
        var btcAvailable = me.accountObject.BTCAvailable()
        var total = (btcAvailable / slotsRemaining) 
        total = functions.subtractPercent(total,.1)
      // if we have BTC available, purchase the coin
        if(total > .0001){
            var newHolding = createManagedHolding(data.pair,data.endDate || new Date(),price,total,undefined,orderStageEnum.forcePurchase,tier )
            simLog( "Buy " + newHolding.coin + " @ " + price, newHolding.buyDate)
            txLogToFile("Buy " + newHolding.coin + " - " + getHoldingInfoForLogFile(newHolding))
            me.managedHoldings.push(newHolding)
            
          /** if we are running sim mode, update the BTCAvailable and sim variables */
            if(isSim()){
              me.accountObject._BTCAvailable -= total
              algo.simVariables.vMaxHoldings = Math.max(algo.simVariables.vMaxHoldings, me.accountObject.managedHoldingsCount())
            }else{
          /** if not in sim mode, immediately call the function that completes
               * the real purchase  */
              saveManagedHoldings()
              maintainHoldings.loop()
            }
        }
    }
}
function dumpCoin(holding, date){
  if(!date){date = new Date()}
  simLog("Sell " + holding.coin, date)
  holding.stage = orderStageEnum.dump
  holding.sellDate = date
  txLogToFile("Sell " + holding.coin + " - " + getHoldingInfoForLogFile(holding))
  
  
  if(isSim()){ // if sim mode, update BTC holdings
    me.accountObject._BTCAvailable += holding.currentBTCValue
    me.accountObject.minBTCHeld = Math.min(me.accountObject.minBTCHeld, me.accountObject._BTCAvailable)
    
    me.accountObject.maxBTCHeld = Math.max(me.accountObject.maxBTCHeld, me.accountObject._BTCAvailable)
    maintainHoldings.moveHoldingToHistory(holding)
  }else{
    maintainHoldings.loop()
  }
}

function createBuySignal(pair, price, total){
  /** manually add a buy signal   //createBuySignal("BTC_LBC",.000157)  */
  if(!total){total = me.accountObject.BTCAvailable()}
  var order = createManagedHolding(pair, new Date(),price, total, undefined, orderStageEnum.forcePurchase,1)
  me.managedHoldings.push(order)
  saveManagedHoldings()
  maintainHoldings.loop()
}

function createManagedHolding(coinOrPair,bDate, price, total, orderId, stage, tier, noSlippage, maxGain, minGain){
  /** this method would create or restore a managed holding object with all it's properties 
   * orderId is the orderId returned from poloniex when a coin is purchased
   */
  /** set pair regardless if a coin or pair was passed in first param. */
  
  if(coinOrPair.pair){
/** check if passing the managedholding from disk */

  }
  var t = "tier" + Math.floor(tier)
  var slip = algo.simVariables.slippageAmount
  var sll = algo.omParams[t].stopLossLevels
  var buyTier = tier || 1.1
  var managedHolding =  {
    /** pair base coin will be BTC in most cases, unless coin is BTC, in which
    * case the pair will be USDT_BTC */
    buyTier: buyTier,
    amount: total / price,
    buyDate: bDate,
    sellDate: undefined,
    sellReason: "",
    base: "",
    coin: "",
    currentGain: 0,
    currentGainPercent: 0,
    currentBTCValue: total,
    /** TODO use the following to sell stagnant trades */
    lastGainDate: bDate,  //TODO last date at which we exceed maxGain
    maxGain: maxGain || -10,
    minGain: minGain || 100,
    maxPrice: 0,
    peaks_p1: 0,
    peaks_d1: 0,
    peaks_p2: 0,
    orderId: orderId,
    omParams: algo.omParams[t],
    oPrice: price,
    oAmount: total / price,
    oBTCValue: total,
    pair: "",
    price: price,
    priceHistory: "00000",
    purchaseAttempts: 0,            //keep track of how many times we have tried to purchase this
    purchaseAttemptDate: bDate,     //keep track of last time purchase attempt was sent
    slippageAmount: slip,
    stage: stage, 
    stopLossLevel: 1,
    stopLossLevels: sll,
    xf_BTCValue: function(){return this.xf_fixNum(this.price * this.amount)},
    xf_LossGain: function(){return this.xf_fixNum(this.xf_BTCValue() - this.oBTCValue)},
    xf_PLG: function(){
      var gain = this.xf_fixNum(((this.currentBTCValue - this.oBTCValue)/this.oBTCValue)* 100)
      if (gain > this.maxGain){this.maxGain = parseFloat(gain)}
      if (gain < this.minGain){this.minGain = parseFloat(gain)}
      return parseFloat(gain)
    },
    xf_setAmount: function(amount){
      var amountDiff = Math.abs(this.amount - amount)
      this.amount =  amount
      this.xf_doSlippage(amountDiff)
    },
    xf_setPair: function(coinOrPair){
      this.pair = coinOrPair.includes("_")?coinOrPair:getPairForCoin(coinOrPair)
      this.coin = getCoinFromPair(this.pair)
      this.base = getBaseFromPair(this.pair)
    },
    xf_setPrice: function(price){
      this.price = price
      if(price > this.maxPrice){
        this.maxPrice = price
      }
      this.xf_setValues()
    },
    xf_setValues: function(){
      this.currentBTCValue = this.xf_BTCValue()
      this.currentGainPercent = this.xf_PLG()
      this.currentGain = this.xf_LossGain()
    },
    xf_fixNum: function(num){
      return parseFloat(parseFloat(num).toFixed(8))
    },
    xf_doSlippage: function(amount){
      if(!globals.orderManagerSimulationMode){return}
      if(!amount){amount = this.amount}
      this.amount -= (amount * (this.slippageAmount / 100) )
      this.xf_setValues()
    },
    xf_init: function(coinOrPair,bDate, price, total, orderId, stage){
      this.xf_setPair(coinOrPair)
      if(!noSlippage){
        this.xf_doSlippage()
      }
    }

  }
  
  managedHolding.xf_init(coinOrPair,bDate, price, total, orderId, stage)
  return managedHolding
  
}
function saveManagedHoldings(){
  db.saveManagedHoldings(globals.debugUserName,me.managedHoldings)
}
function loadManagedHoldings(){
  db.loadManagedHoldings(globals.debugUserName,function(err,result){
    if(result[0].managedHoldings){
      var holdings =  JSON.parse(result[0].managedHoldings)
      holdings.forEach(function(holding){
        me.managedHoldings.push(
          //TODO loaded holding prices do not match as when saved
          createManagedHolding(holding.pair,holding.buyDate,holding.oPrice,holding.oBTCValue,undefined,holding.stage,holding.buyTier,true,holding.maxGain)
        )
      })
    }
  })
  if(!isSim()){
    db.loadManagedHoldingsHistory(moment().subtract(4,"hours").toDate(),function(err,result){
      if(result.length > 0){
        me.managedHoldingsHistory = result
      }
    })
  }
}

/** Loop Functions for Sim and Backtest */
function doLearningLoop(){
    me.initBacktestOrderManagerSimulation(function(){

      me.managedHoldingsHistory = []
      me.accountObject.transactions = []
      me.accountObject._BTCAvailable = 10
      me.managedHoldings = []

        var sl = getSLLevel()
        if(me.simBestBTCLevel < me.accountObject.simResult.endBTC){
          me.simBestBTCLevel = me.accountObject.simResult.endBTC
          me.simBestSLLevel = sl
        }
        sl += .1
        if(sl > 5){
          //set old row to BEST stoploss level
          setSLLevel(me.simBestSLLevel)
          //get current percent level
          var pc = getSLLevel(true)
          //increment half a percent
          pc += .5
          var slls = algo.omParams.tier1.stopLossLevels
          if(pc <=20){
            //and add the new row
            slls.push({percent: pc, stopLoss: .5})
          }else{
            // pc over 20, we are done
            slls = slls
          }
        }else{
          setSLLevel(sl)
        }
        doLearningLoop()

    })
    function setSLLevel(sl){
      levels = algo.omParams.tier1.stopLossLevels
      levels[levels.length-1].stopLoss = sl
    }
    function getSLLevel(percent){
      levels = algo.omParams.tier1.stopLossLevels
      if(percent){
        return levels[levels.length-1].percent
      }else{
        return levels[levels.length-1].stopLoss
      }
    }
}
function runBackTestLoop(groupNum, groups,callback){
    var start = groupNum * algo.simVariables.backtestBlockLoadSize
    var count = algo.simVariables.backtestBlockLoadSize
    console.log("Getting backtest Group: " + (groupNum+1) + " of " + (groups+1) + "  total time: " + algo.simVariables.backtestTotalTime)
    //load the rows in groups of 10,000, and send them in sequence to simulate real trades
    functions.timeEvent(true)
    db.getBacktestRows(start, count,algo.simVariables.backtestDaysStart,algo.simVariables.backtestDaysEnd,
                        algo.simVariables.backtestDateStart,algo.simVariables.backtestDateEnd,
                        groupNum, function(res,groupNum){
      if(res.length > 0){
        res.forEach(function(item, index){
          me.analyseCryptoReport(item, index + (groupNum * algo.simVariables.backtestBlockLoadSize)   )
        })

        groupNum += 1
        algo.simVariables.backtestTotalTime += functions.timeEvent()
        if(groupNum <= groups){
          res=undefined
          runBackTestLoop(groupNum,groups, callback)
        }else{
          functions.timeEvent()
          console.log("Backtest simulation end - Time: " + algo.simVariables.backtestTotalTime)
        
          //sell any remaining holdings
          me.managedHoldings.forEach(function(holding){
            dumpCoin(holding)
          })

          var gainloss = 0
          var maxGain = 0
          me.accountObject.transactions.forEach(function(item){
            gainloss += item.gainLoss
            maxGain += item.maxGain
          })

          me.managedHoldingsHistory.sort(function(a,b){ return new Date(a.buyDate) - new Date(b.buyDate)})
          
          me.accountObject.simResult = {
            startBTC: 10,
            endBTC: me.accountObject.BTCAvailable(),
            minBTCHeld: me.accountObject.minBTCHeld,
            maxBTCHeld: me.accountObject.maxBTCHeld,
            maxholdings: algo.simVariables.vMaxHoldings,
            totalGain: gainloss, 
            totalMaxGain: maxGain,
            variables: algo.omParams,
            simVariables: algo.simVariables,
            transactions: me.managedHoldingsHistory,
            log: me.accountObject.simLog
          }
          
          console.log(me.accountObject.simResult)
          
          
          /** TODO - check bitcoin status if going up or down
           * don't buy coins when bitcoin increasing
           */
db.saveBacktestSimResult(me.accountObject.simResult, function(err,res){
            console.log(err)
            
            if(callback){
              callback()
            }else{
              process.exit()
            }
          })
          
          //me.simResult = me.simResult
        }
      }
    })
}
/** End Loop Functions for Sim and Backtest */


/** general functions */
function getHoldingsFromPolo(/** Strength:1 to 4 */strength){
    strength = strength || 4

    /** put in a sequence of requests, to hopefully get a fast update */
    for (var i = 0; i <= strength-1; i++) {
       var len = 2000 + (i*1010)
       polon.polo_methods.getHoldings(len)
       polon.polo_methods.getOrders(len + 250)
    }
}
function isSim(){
  return me.accountObject.sim
}
function simLog(text, date){
  if(isSim()){
    me.accountObject.simLog += moment(date).utc().format('MMMM Do YYYY, H:mm:ss') + " -- " + text + String.fromCharCode(13, 10)
  }
  functions.speak(text,date)
}
function getBaseFromPair(pair){
  return pair.split("_")[0]
}
function getCoinFromPair(pair){
  return pair.split("_")[1]
}
function getPairForCoin(coin){
  if (coin != "BTC"){
    return "BTC_" + coin
  }else{
    return "USDT_" + coin
  }
}
var orderStageEnum = {
  /**                   actively purchase this coin - this means passively buying and
   *                    avoiding the taker fee, so buy order price must be constandly adjusted
   *                    to be just above the orderbook high bid */
  purchase: 0,

  /**                   immediate purchase - place bid x% above low ask to ensure purchase */
  forcePurchase: 1,

  /**                   coin has been purchased, and orderId is set from orders */
  purchaseComplete: 2,  
  /**                   purchased, and confirmed against orders */
  purchasedConfirmed: 3,

  /**                   actively selling this coin - this means passively selling and
   *                    avoiding the taker fee, so sell order price must be constandly adjusted
   *                    to be just below the orderbook low ask */
  sell: 8,

  /**                   actively dumping this coin - sell at x% below high bid to ensure sale */
  dump: 9,

  /**                   coin has been sold, and orderId no longer exists in orders */
  
  sold: 10
}

function txLogToFile(data){
  if(isSim()){return}
  var now = new Date();
  var fileNow = now.toDateString().split(" ").join("_") + '.txt'
  var newline = String.fromCharCode(13, 10);
  var logfile = './logs/tx-' + fileNow 

  var date = moment().utcOffset('-0700').format('MMMM Do YYYY, h:mm:ss a')

  fs.appendFile(logfile, newline +  date + "--" + data,function(err){

  } )
}
function logToFile(data, filename){
  var now = new Date()
  var fileNow = now.toDateString().split(" ").join("_") + '.txt'
  var newline = String.fromCharCode(13, 10);
  var logfile = './logs/' + filename + "_" + now.toDateString().split(" ").join("_") + '.txt'
  var date = moment().utcOffset('-0700').format('MMMM Do YYYY, h:mm:ss a')

  fs.appendFile(logfile, newline +  date + "--" + data,function(err){

  } )

}
function getHoldingInfoForLogFile(holding){
  return "Tier: " + holding.buyTier + "   " +
         "buyDate: " + holding.buyDate + "   " +
         "sellDate: " + holding.sellDate + "   " +
         "oPrice: " + holding.oPrice + "   " +
         "price:" + holding.price + "   " +
         "oBTCValue: " + holding.oBTCValue + "   " +
         "BTCValue: " + holding.currentBTCValue + "  " +
         "gain: " + holding.currentGainPercent + "%   " +
         "minGain: " + holding.minGain + "   " +
         "maxGain: " + holding.maxGain + "   " 
}















function collapseAndHideOldComments(){
  //   1:Object {available: "0.41604167", onOrders: "0.00000000", btcValue: "0.41604167"}
  // available:"0.41604167"
  // btcValue: ()=>{price * amount}
  // }
  // function createOrder(pair, price, amount, type){
  //   0:"BTC_DGB"
  // 1:Array[1] [Object]
  // length:1
  // __proto__:Array[0] []
  // 0:Object {orderNumber: "10333659022", type: "sell", rate: "0.00002001", …}
  // amount:"1950.64473498"
  // date:"2017-06-29 01:22:55"
  // margin:0
  // orderNumber:"10333659022"
  // rate:"0.00002001"
  // startingAmount:"1950.64473498"
  // total:"0.03903240"
  // type:"sell"
}