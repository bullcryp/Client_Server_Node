ALTER TABLE `polodb`.`xmr_zec` 
DROP COLUMN `attributes`,
ADD COLUMN `startTradeId` INT NULL AFTER `endDate`,
ADD COLUMN `endTradeId` INT NULL AFTER `startTradeId`,
ADD COLUMN `high` DECIMAL(13,8) NULL AFTER `endTradeId`,
ADD COLUMN `low` DECIMAL(13,8) NULL AFTER `high`,
ADD COLUMN `open` DECIMAL(13,8) NULL AFTER `low`,
ADD COLUMN `close` DECIMAL(13,8) NULL AFTER `open`,
ADD COLUMN `sell_price` DECIMAL(13,8) NULL AFTER `close`,
ADD COLUMN `sell_amount` DECIMAL(13,8) NULL AFTER `sell_price`,
ADD COLUMN `sell_total` DECIMAL(13,8) NULL AFTER `sell_amount`,
ADD COLUMN `sell_trades` INT NULL AFTER `sell_total`,
ADD COLUMN `buy_price` DECIMAL(13,8) NULL AFTER `sell_trades`,
ADD COLUMN `buy_amount` DECIMAL(13,8) NULL AFTER `buy_price`,
ADD COLUMN `buy_total` DECIMAL(13,8) NULL AFTER `buy_amount`,
ADD COLUMN `buy_trades` INT NULL AFTER `buy_total`;