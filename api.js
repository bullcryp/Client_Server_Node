var db = require('./db/db')
var app = require('./app').app
var moment = require('moment')
var globals = require('./globals')
var _ = require('lodash');


/** redirect to the renamed index file */
app.get('/', function(req, res) {
  try{
    res.sendFile(__dirname+'/web/index_api.html');
  }catch(e){}
})

app.get('/api-list', function(req,res){
  res.sendfile(__dirname+'/apis.html')
})

app.get('/publickey',function(req,res){
  res.json({pkey: "ksdfj"})
})

app.get("/returnPairs", function(req,res){
  try{
    res.json(globals.pairSets)
  }catch(e){
    res.end("No Data")
  }
})

app.get("returnBacktest/:pair", function(req,res){

})

app.get("/returnPair",function(req,res){
  
  try{
    var pair = req.query.pair, start = req.query.start, end = req.query.end
    if(!req.query.pair){pair="BTC_STR"} //TODO remove debug data
    if(!req.query.start){start = moment.utc().subtract(2,"hours"); end = moment.utc().add(1,"hour")}
      db.getTradePairForBackTest(pair,start, end,function(err,data){
        processData(res,err,data)
      })
  }catch(e){}
})

app.get("/returnPairTradesCount/:pair",function(req,res){
  try{
    db.getTradePairTradesCount(req.params.pair, function(err,data){
      processData(res,err,data)
    })
  }catch(e){}
})
app.get("/getOldestTrade/:pair",function(req,res){
  try {
    db.getTradePairOldestTrade(req.params.pair, function(err,data){
      processData(res,err,data)
    })
  } catch (e){} 
})


var processData = function(res,err,data){
  try{
    if(err){
      res.json(err)
    }else if(data.length < 1){
      res.end("No Data")
    }else{
      res.json(data)
    }
  }catch(e){}
}


