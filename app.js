
//poloniex API startup object module

var globals = require('./globals.js')

var express = require('express')
var app = exports.app = express()
var http = require('http').Server(app)



// require("./api")

//require("./workers/workers")

// var crypto = require('crypto')
// var sec = 'de3080b8ee4eced3a08bcebc88d1d6a6caeeff64c5afa72a648e3c9302cc28f4f6dc16335dadfa1c8bb7f0faf21ca76592336aa57f68d756fca8a91d0cb0408b'
// var paramString = "account=all&command=returnCompleteBalances&nonce=function%20()%20%7B%0A%20%20%20%20var%20now%20%3D%20Math.pow(10%2C%207)%20*%20new%20Date()%0A%0A%20%20%20%20if%20(now%20%3D%3D%20last)%20%7B%0A%20%20%20%20%20%20repeat%2B%2B%0A%20%20%20%20%7D%20else%20%7B%0A%20%20%20%20%20%20repeat%20%3D%200%0A%20%20%20%20%20%20last%20%3D%20now%0A%20%20%20%20%7D%0A%0A%20%20%20%20var%20s%20%3D%20(now%20%2B%20repeat).toString()%0A%20%20%20%20return%20%2Bs.substr(s.length%20-%20length)%0A%20%20%7D"
// console.log( crypto.createHmac('sha512',sec).update(paramString).digest('hex') )

//we don't need any routes, just set /web to static
app.use(express.static("web"))

/***  socketio connections  */
var io = require('socket.io')(http)

//client socket to connect to trade server
// var clientIo  = require('socket.io-client');
// const clientSocket = clientIo.connect(globals.tradeServerConnectString(), { /*secure: true,*/ reconnect: true, rejectUnauthorized: false });

// exports.io = io
// exports.clientIo = clientIo
// exports.clientSocket = clientSocket
/*** end socketio connections */

// require('./socketio.js')  //this is the ./socketio.js file - just attach without setting variable
var polon = require('./polo_functions.js')
var functions = require('./functions.js')
//var db = require("./db/db.js")

var bodyParser = require('body-parser')
app.use(bodyParser.json())
    

/** get command line switches */
var myArgs = process.argv.slice(2)
// console.log(myArgs)

if (myArgs.includes("nodatadisplay")){
    globals.tradeCollectionConsoleOutput = false
}
    
    

//start the web server
// http.listen(globals.HTTPport, ()=>{

//     functions.jettyClear()
//     console.log('listening on *:' + globals.HTTPport);
    
// })

/** start the intervals to get Holdings, Orders, and Margin Data */
polon.exchange_methods.startAccountInfoIntervals() 