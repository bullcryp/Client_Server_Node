

var moment = require('moment')
var functions = require('../functions')
var globals = require('../globals')
var _ = require('lodash');
var algo = require('../algo')
// var fs = require("fs");

// var mysql = require('mysql')

var async = require('async')

/** connect to mongo  and/or mysql*/
// if (functions.isLocal()) {
//   database_host = "localhost"
// } else {
//   hostName = require('os').hostname()
//   if (hostName == 'ip-172-31-21-207') {
//     database_host = "localhost"
//   } else {
//     database_host = "localhost"
//   }
// }


// var remoteConn = mysql.createConnection({
//   host: '52.42.10.34',
//   user: 'polo_user',
//   password: 'HeaVenLy',
//   database: "polodb",
//   multipleStatements: true,
//   timezone: 'utc'
// });
// if(functions.isLocal()){
//   remoteConn.connect(function (err) {
//     if (err) {
//       console.error('error connecting: ' + err.stack);
//       return;
//     }
//     console.log('connected as id ' + connection.threadId);
//   });
// }
/** conect to mysql */

// var connection = mysql.createConnection({
//   host: database_host,
//   user: 'polo_user',
//   password: 'HeaVenLy',
//   database: "polodb",
//   multipleStatements: true,
//   timezone: 'utc'
// });
// connection.connect(function (err) {
//   if (err) {
//     console.error('error connecting: ' + err.stack);
//     return;
//   }
//   console.log('connected as id ' + connection.threadId);
// });

/** export our models */
var me = module.exports = {
  con: connection,

  getTradePairLastInfo: function (tradePairHistory, callback) {
    //get last date from an order pair order orderbook

    var sql = `select endTradeId as eti, endDate as edt from ` + tradePairHistory.pair + ` ORDER BY startDate DESC LIMIT 1;`
    makeSQLCall(sql, function (err, res) {
      if (!err){
        try {
          callback(functions.utc(res[0].edt), res[0].eti, tradePairHistory)
        } catch (e) {
          callback(functions.unixTimeStampToTime(1480000000), 0, tradePairHistory)
        }
      }
    })
  },
  getTradePairSlices: function (pair, start, count, extraCallbackParam, callback) {

    // fixSQLTables(pair)

    var sql = `select * from ` + pair + ` ORDER BY endTradeId DESC LIMIT ` + start + `,` + count + `;`
    // console.log(sql)
    makeSQLCall(sql, function (err, res) {
      if (err) {
        if(err.message.includes("ER_NO_SUCH_TABLE")){
          var sql2 = "CREATE TABLE " + pair + " LIKE BTC_AMP;"
          makeSQLCall(sql2, function(err,res){
            
          })
          callback("",[],extraCallbackParam)
          // globals.pairSets = globals.pairSets.filter(item => item != pair)
        }
      } else {
        // callback(res[0].edt,res[0].eti,tradePairHistory)
        var trades = []
        res.forEach(function (item, index) {
          trades.push(makeAggregate(item))
        })
        callback("", trades, extraCallbackParam)
      }
    })

    function makeAggregate(item) {
      return {
        dateStart: item.startDate,
        dateEnd: item.endDate,
        startTradeId: item.startTradeId,
        endTradeId: item.endTradeId,
        high: item.high,
        low: item.low,
        close: item.close,
        sell: {
          price: item.sell_price,
          amount: item.sell_amount,
          total: item.sell_total,
          trades: item.sell_trades
        },
        buy: {
          price: item.buy_price,
          amount: item.buy_amount,
          total: item.buy_total,
          trades: item.buy_trades
        }

      }
    }

  },
  getTradePairSlicesByDate: function(pair, start, end){
    var sql = `select * from ` + pair + ` ORDER BY endTradeId DESC LIMIT ` + start + `,` + count + `;`
    // console.log(sql)
    makeSQLCall(sql, function (err, res) {
      if (err) {
        if(err.message.includes("ER_NO_SUCH_TABLE")){
          //delete pair from db, and from memory
          var sql2 = "CREATE TABLE " + pair + " LIKE BTC_AMP;"
          makeSQLCall(sql2, function(err,res){

          })
          // globals.pairSets = globals.pairSets.filter(item => item != pair)
        }
      } else {
        // callback(res[0].edt,res[0].eti,tradePairHistory)
        var trades = []
        res.forEach(function (item, index) {
          trades.push(makeAggregate(item))
        })
        callback("", trades, extraCallbackParam)
      }
    })
  },
  getTradePairForBackTest: function (pair, callback) {

    me.getBacktestLastInfo(pair, function(startDate){

          /** start included */
        var  where = " WHERE startDate > '" + startDate + "'"
        
        
        var sql = `SELECT * FROM ` + pair + where + ` ORDER BY startDate ASC`

        makeSQLCall(sql, function (err, res) {
          if (!err) {
            callback(res)
          }
        })

    }, callback)

  },
  makeBacktestClone: function(callback){
    var sql ="truncate polodb.backtestscopy;"
    var sql2  = "insert into polodb.backtestscopy (testDate, startDate, endDate, results, pair, price)  select testDate, startDate, endDate, results, pair, price from backtests order by enddate asc;"
    makeSQLCall(sql, function (err, res) {
          if (!err) {
            makeSQLCall(sql2, function (err, res) {
              if (!err) {
                callback()
              }
            })
          }
        })
  },
  getBacktestTotalRows: (startDays,endDays, startDate, endDate, callback)=>{
    var sql
    // startDate = moment(startDate).toDate()
    if(startDate == "" || endDate == ""){
      sql = "select count(*) as count from polodb.backtests  where endDate > DATE_SUB(NOW(), INTERVAL " + startDays + " day) AND endDate < DATE_SUB(NOW(), INTERVAL " + endDays + " day)"
    }else{
      sql = "select count(*) as count from polodb.backtests  where endDate > '" + startDate + "' AND endDate < '" + endDate + "'"
    }
    makeSQLCall(sql, function (err, res) {
          if (!err) {
            callback(res[0].count)
          }
        })
  },
  getBacktestRows: (startRow,count,startDays,endDays,startDate, endDate, extraParam, callback)=>{
    var sql
    if(startDate == "" || endDate == ""){
      sql = "select * from polodb.backtests  where endDate > DATE_SUB(NOW(), INTERVAL " + startDays + " day) AND endDate < DATE_SUB(NOW(), INTERVAL " + endDays + " day) LIMIT " + startRow + "," + count
    }else{
      sql = "select * from polodb.backtests  where endDate > '" + startDate + "' AND endDate < '" + endDate + "' LIMIT " + startRow + "," + count
    }
    makeSQLCall(sql, function (err, res) {
      if (!err) {
        callback(res, extraParam)
      }else{
        console.log(err)
      }
    })

  },
  getBacktestLastInfo: (pair, callback)=>{
    var sql = `select startDate from polodb.backtests where pair = '` + pair + `' ORDER BY startDate DESC LIMIT 1;`
    makeSQLCall(sql, function (err, res) {
      if (!err  && res.length > 0){
          callback(functions.sqlDate(res[0].startDate))
      }else{
          callback( functions.sqlDate(functions.unixTimeStampToTime(1480000000)) )
      }
    })
  },
  cleanOldData: ()=>{
    if( functions.isLocal() || functions.isLocal("172-31-21-207")  ){  //check if IP of EC2 which will keep our backtest data
      return
    }
    
    var sql = []
    var days = Math.min(globals.DBDataLengthInWeeks * 7,globals.marketTradeHistoryLength)
    var cutDate = functions.sqlDate( moment().subtract(days,"days").toDate() )
    console.log("Running clean data older than " + cutDate)
    sql.push("delete from polodb.backtests where endDate < '" + cutDate + "';optimize table polodb.backtests")
    
    globals.pairSets.forEach(function(pair){
      sql.push("delete from polodb." + pair + " where endDate < '" + cutDate + "';optimize table polodb." + pair + ";")
    })
    
    sql.forEach(function(sqi){
      makeSQLCall(sqi, function(err,res){
        if(err){
          console.log(err.message)
        }
        functions.log("Cleaned data - " + sqi)
      })
    })
  },
  updateBacktestData: function (pair, Data, callback) {
    var rows = Data.length - 1
    var row = 0
    var masterCallback = callback
    var log

    writeRows(row, callback)
    
    function writeRows(row) {
      if(row > rows){
        masterCallback()
      }else{
        var element = JSON.parse(JSON.stringify(Data[row]))
        var sd = functions.sqlDate(element.result.startDate)
        var ed = functions.sqlDate(element.result.endDate)
        var price = element.result.price
        delete element.result.startDate
        delete element.result.endDate
        var sql = `INSERT INTO backtests (testDate,startDate, endDate, pair, price, results) 
                    VALUES ('`+ sd + `','` + sd + `','` + ed + `','` + pair + `','` + price + `','`+ JSON.stringify(element) + `');`
        
        writeRow(sql, row, writeRows)
        
      }
    }

    function writeRow(sql, row, callback) {
      connection.query(sql, function (err, res) {
        if (err) {
          console.log(err)
        }
        row += 1
        callback(row)
      })
    }
  },
  saveLiveHitResults: function(pair,price, results){
    var sql = `INSERT INTO 1LIVEHITRESULTS (hitDate,pair,price, results)
          VALUES ('` + functions.sqlDate(new Date()) + `','` + pair + `','` + price + `','` + JSON.stringify(results) + `');`

    makeSQLCall(sql)
  },
  saveBacktestSimResult: function(r, callback){
    var simStartDate = r.simVariables.backtestDateStart || moment().subtract(r.simVariables.backtestDaysStart*24, "hours").toDate()
    var simEndDate = r.simVariables.backtestDateEnd || moment().subtract(r.simVariables.backtestDaysEnd*24, "hours").toDate()

    var sql = `INSERT INTO 1orderManagerSimResults 
                (simStartDate, simEndDate, startBTC,endBTC,minBTC,maxBTC,maxHoldings, totalGain, 
                totalMaxGain, variables, simVariables, transactions)
        VALUES ('` + functions.sqlDate(simStartDate) + `','` +
                      functions.sqlDate(simEndDate) + 
                      `','` + r.startBTC + 
                      `','` + r.endBTC + `','`+ r.minBTCHeld + `','` + r.maxBTCHeld + `','` + r.maxholdings + 
                      `','` + r.totalGain + `','` + r.totalMaxGain + 
                      `','` + JSON.stringify(r.variables, function(key,val){
                                    if (typeof val === 'function') {
                                      return `` + val;
                                    }
                                    return val;
                      }).replace(/\\n/g,"") +
                      `','` + JSON.stringify(r.simVariables) +
                      `','` + JSON.stringify(r.transactions) + `');`

    //makeSQLCall(sql, callback)
    // makeSQLCallRemote(sql,function(err,res){
    //   console.log(err)

    // })

    
  },
  saveManagedHoldings: function(user,managedHoldings){
    var sql = `INSERT INTO 1USERDATA (user,managedHoldings)
                VALUES ('` + user + `','` + JSON.stringify(managedHoldings) + `')
                ON DUPLICATE KEY UPDATE managedHoldings = '` + JSON.stringify(managedHoldings) + `';`
          makeSQLCall(sql)
  },
  loadManagedHoldings: function(user, callback){
    var sql = `SELECT managedHoldings FROM 1USERDATA WHERE user = '` + user + `';`
    makeSQLCall(sql,callback)
  },
  // saveManagedSpreads: function(user,managedHoldings){
  //   var sql = `INSERT INTO 1USERDATA (user,managedHoldings)
  //               VALUES ('` + user + `','` + JSON.stringify(managedHoldings) + `')
  //               ON DUPLICATE KEY UPDATE managedHoldings = '` + JSON.stringify(managedHoldings) + `';`
  //         makeSQLCall(sql)
  // },
  // loadManagedHoldings: function(user, callback){
  //   var sql = `SELECT managedHoldings FROM 1USERDATA WHERE user = '` + user + `';`
  //   makeSQLCall(sql,callback)
  // },
  saveManagedHoldingsHistory: function(holding,callback){
    var sql = `INSERT INTO 1managedHoldingsHistory (buyDate,sellDate,pair,holding)
                VALUES ('` + functions.sqlDate(holding.buyDate) + `','` + functions.sqlDate(holding.sellDate) + `','` + holding.pair + `','` + JSON.stringify(holding) + `');`
                // ON DUPLICATE KEY UPDATE managedHoldings = '` + JSON.stringify(managedHolding) + `';`
          makeSQLCall(sql,callback)
  },
  loadManagedHoldingsHistory: function(buyDate,callback){
    buyDate = functions.sqlDate(buyDate || new Date())
    var sql = `SELECT holding from polodb.1managedHoldingsHistory WHERE buyDate >= '` + buyDate  + `';` 
    makeSQLCall(sql,callback)
  },
  saveUserData: function(user,data){
          
          var sql = `INSERT INTO 1USERDATA (user,data)
                VALUES ('` + user + `','` + data + `')
                ON DUPLICATE KEY UPDATE data = '` + data + `';`
          makeSQLCall(sql)
  },
  getUserData: function(user,callback){
    var sql = `SELECT data FROM 1USERDATA WHERE user = '` + user + `';`
    makeSQLCall(sql,callback)
  },
  sqlQ: async.queue(function (task, callback) {
    connection.query(task.sql, task.values,
      function (err, res) {
        if (err) {
          console.log(err)
        }
        try {
          callback()
        }catch(e){ console.log(e) }
      }
    )
  }, 4),
  updateTradePair: function (pair, Data) { //attempts to update existing pair record in the db

    var values = []
    
    for (var i = Data.length - 1; i >= 0; i--) {
      var item = Data[i]
      values.push([item.dateStart,  //globals.moment(item.dateStart).format('YYYY-MM-DD HH:mm:ss')
      item.dateEnd,  // globals.moment(item.dateEnd).format('YYYY-MM-DD HH:mm:ss')
      item.startTradeId,
      item.endTradeId,
      item.high,
      item.low,
        null,
      item.close,
      item.sell.price,
      item.sell.amount,
      item.sell.total,
      item.sell.trades,
      item.buy.price,
      item.buy.amount,
      item.buy.total,
      item.buy.trades
      ])
    }
    var task = { 
                  sql: `INSERT INTO ` + pair + ` (startDate, endDate, startTradeId, endTradeId, high, low, open, close, sell_price, sell_amount, sell_total, sell_trades, buy_price, buy_amount, buy_total, buy_trades ) VALUES ?`,
                  values: [values] ,
                  callback: function(data){}
                }

   

    me.sqlQ.push(task, function(err) {
        // console.log('finished processing mysql');
    });

  },
  getPairSets: function (callback) {
    var sql = "SELECT pairs FROM 1pairsets"
    makeSQLCall(sql,
      function (err,res) {
        var mapped = _.map(res,"pairs")
        // if(mapped.length > 0){
          // globals.pairSets = mapped
          globals.pairSets = [...new Set([...globals.pairSets ,...mapped])]
          // console.log(mapped)
          if(callback){callback()}
          
      }
    )

  },
  savePairSets: function (pairs) {
        
      pairs.forEach(function(item, index){
        var sql = `INSERT INTO 1pairsets (pairs) VALUES ("` + item + `")`

        makeSQLCall(sql, function(err,res){
          // if(err){
          //   console.log(err)
          // }
        })
      })
      
  },

  fixSQLTables: function(){
    /** temp method to correct tables */
    try{
      // me.getPairSets(function(){

        var sql = []
      //   //add the liveanalysehitresults table
        // sql.push( "CREATE TABLE `polodb`.`1managedHoldingsHistory` (buyDate DATETIME, sellDate DATETIME, pair VARCHAR(12),holding JSON, UNIQUE KEY(buyDate, pair) ) ;")

        // sql.push("ALTER TABLE `polodb`.`1orderManagerSimResults` CHANGE COLUMN `totalMaxGain` `totalMaxGain` DECIMAL(8,4) NULL DEFAULT NULL")
      //   sql.push("ALTER TABLE `polodb`.`1USERDATA` CHANGE COLUMN `data` `data` LONGTEXT NULL DEFAULT NULL ;")
        
      //   // var columns = ["high", "low", "open", "close", "sell_price", "sell_amount", "sell_total", "buy_price", "buy_amount", "buy_total"]
      //   // globals.pairSets.forEach(function(pair){
      //   //   columns.forEach(function (column) {
      //   //     sql.push("ALTER TABLE `polodb`.`" + pair + "` CHANGE COLUMN `" + column + "` `" + column + "` FLOAT NULL DEFAULT NULL")
      //   //   })
      //   // })
        
        sql.forEach(function(sqi){
          makeSQLCall(sqi, function(err,result){

          })
        })
        

      // })
      

    }catch(e){
      console.log(e)
    }

    
  }


}  //end module exports

var glbSql;
function makeSQLCall(sql,callback){
  glbSql = sql
  connection.query(sql,
    function (err, res) {
      if(err && !callback){
        console.log(err)
      }
      if(callback){
        callback(err,res)
      }
    }
  )
}
function makeSQLCallRemote(sql,callback){
  glbSql = sql
  remoteConn.query(sql,
    function (err, res) {
      if(err && !callback){
        console.log(err)
      }
      if(callback){
        callback(err,res)
      }
    }
  )
}


