//import { hostname } from 'os';
//import { text } from '../../../Library/Caches/typescript/2.6/node_modules/@types/body-parser';

//var say = require('say')
//var polon = require('./polo_functions.js');
//var globals = require('./globals')

//var moment = require('moment')


//var os = require('os')

//var Jetty = require("jetty")
//var jetty = new Jetty(process.stdout)

//var _ = require('lodash')

var modules = require('./modules')
/**  variables */

var timeEvent_start

var speaking = false

String.prototype.replaceAll = function(s, r) {
    var t = this;
    return t.replace(new RegExp(s, 'g'), r);
  }

var me = module.exports={
    words:[],
    speak: (word,logDate, voice, speed)=>{
        if(modules.os.hostname().indexOf("Stefs") < 0){return}
         
        var voices = ["Bad news", "Bah", "Bells", "Bubbles", "Cellos", "Deranged","Good News", "Hysterical", "Pipe Organ", "Whisper" ]
        voice = "" //voices[ getRandomInt(0,voices.length-1) ]
        if(word){

            me.words.push(word)
        }

        try{
            if(modules.globals.disableSpeech){
                var word = me.words[0]
                me.log(word, logDate)
                done(0)
                return
            }
            if(!speaking){
                if(me.words.length > 0){
                    speaking = true
                    var word = me.words[0]
                    me.log(word)
                    word = word.replace("-","minus ")
                        .replace("ETH","Etherium")
                        .replace("BTC","Bitcoin")
                        .replace("XMR","Monero")
                        .replace("LSK", "Lisk")
                        .replace("FCT", "Fact")
                    word = word.trim()
                    if (!me.isNumeric(word)){
                        modules.say.speak(word, voice,1.3,function(err){
                            done()
                        })
                    }else{
                        // voice = "Bad News"
                        modules.say.speak(word, voice,1.9,function(err){
                            done()
                        })
                        // var digits = word.split("")
                        // digits.forEach(function(item,index){
                        //     if (index != 0  || module.exports.getRandomInt(1,3)==1 || item == "-"){
                        //         if (item == "-"){item="minus"}
                        //         setTimeout(function() {
                        //             say.speak(item, voice,1.8)
                        //         }, index*400)
                        //     }
                        // })
                    }
                }
            }

            
        }catch(e){}

        function done(timeout){
            timeout = timeout || 100
            me.words.shift()
            speaking = false
            if(me.words.length > 0){
                setTimeout(function() {
                     me.speak()
                }, timeout);
            }
        }

    },

    
    isLocal: (hostName)=>{
        var foundHost = false
        console.log("hostname: " + modules.os.hostname())
        if(!hostName){hostName = "Stefs"}
        if(modules.os.hostname().indexOf(hostName)> -1){foundHost = true}
        if(modules.os.hostname().indexOf("robo1")> -1){foundHost = true}
        return foundHost 
    },

    getRandomInt: (min, max)=>{
        return Math.floor(Math.random() * (max - min + 1)) + min;
    },

    setSkipCycles: (skips,mod)=>{
        
        var num = skips.length + mod
        if(num < 0){num = 0}
        if(num > 9){num = 9}
        skips.forEach(function(item,index){
            me.skipCycleCounts[item] -=1
        })

        var arr = [], min=1, max=10
        while(arr.length < num){
            var nextSkipnumber = me.getLowSkipNumber(arr) 
            if(arr.indexOf(nextSkipnumber) > -1){
                continue
            } 
            arr[arr.length] = nextSkipnumber;
            me.skipCycleCounts[nextSkipnumber] += 1
        }
        return arr
    },
    skipCycleCounts: new Array(11).fill(0)
    ,
    getLowSkipNumber: function(arr){
        var scc = me.skipCycleCounts
        var index = 1
        var value = 1000 + (arr[1]?arr[1]:0 )//scc[0]
        for (i=1; i < scc.length; i++){
            if (scc[i] < value && !arr.includes(i)) {
                value = scc[i];
                index = i;
            }
        }
        return index
    },

    isNumeric: (n)=>{
        return !isNaN(parseFloat(n)) && isFinite(n);
    },
    fixPrice(price){
        price = parseFloat(price).toFixed(8)
        return parseFloat(price)
    },

    getPercentIncrease: (v1,v2)=>{
        v1 = parseFloat(v1)
        v2 = parseFloat(v2)
        if( Math.abs(100*(v2-v1)/v1) < .001 ){
            return 0
        }else{
            return 100*(v2-v1)/v1
        }
    },
    getSpreadPercent(bid,ask){
        bid = parseFloat(bid) + .00000001
        ask = parseFloat(ask) - .00000001
        return parseFloat(me.getPercentIncrease(bid,ask).toFixed(4))
    },
    getPercentRemaining: (current, final)=>{
        return (current/final * 100).toFixed(2)
    },

    subtractPercent: (num,percent)=>{
       return parseFloat(  (num - (num / (100/percent))).toFixed(8)  )
    },

    isDate: function(date){
        return modules.moment(date).isValid()
    },

    isPair: (pair)=>{  
        return (modules.globals.pairSets.includes(pair))
    },
    getCoinBase: (pair)=>{
        return pair.split("_")[0]
    },
    getIndexFromPair: (pair)=>{
        return modules.globals.pairSets.indexOf(pair)
    },

    pad: (number, pad)=>{
        if(!pad) pad = "00000"
        if(pad == " ") pad = "                          "
        return (pad+number).slice(-pad.length);
    },

    round: (value, decimals)=>{
        return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
    },
    stringify: function(obj, prop) {
        var placeholder = '____PLACEHOLDER____';
        var fns = [];
        var json = JSON.stringify(obj, function(key, value) {
          if (typeof value === 'function') {
            fns.push(value);
            return placeholder;
          }
          return value;
        }, 2);
        json = json.replace(new RegExp('"' + placeholder + '"', 'g'), function(_) {
          return fns.shift();
        });
        return 'this["' + prop + '"] = ' + json + ';';
    },
    arrayStringContains:(array,text)=>{
        return array.filter(s => s.includes(text)).length > 0
    },
    pushToTopOfArrayWithLimit: (arr,data,limit)=>{ /**push data to array, and limit length of array */
        arr.unshift(data)
        if(limit){
            if (arr.length > limit){
                arr.length = limit
            }
        }
    },
    /** helper method for merging 2 arrays,   */
    mergeArrays: function() {
        return [].concat.apply([], arguments);
    },
    timeEvent: (start, caption)=>{
        var timeStamp = Date.now()/1000;
        if (start){
            timeEvent_start = timeStamp
        }else{
            me.log(caption + (timeStamp - timeEvent_start))
            return timeStamp - timeEvent_start
        }
    },
    getTimeElapsed: (endDate, interval, startDate)=>{
        if(!interval){interval = "seconds"}
        if(!startDate){
            startDate = new endDate()
        }
            return modules.moment.unix(endDate).diff(modules.moment.unix(startDate),interval)
       
    },

    sqlDate: (d)=>{
        return modules.moment.utc(d).format('YYYY-MM-DD HH:mm:ss') //moment(d).format('YYYY-MM-DD HH:mm:ss')
    },

    utc: (t)=>{
        return modules.moment.utc(t).toDate()
    },

    unixTimeStampToTime: (timestamp)=>{
        return modules.moment.utc(modules.moment.unix(timestamp)).toDate()
    },

    timeToUnixTimeStamp: (tm)=>{
        return parseInt(modules.moment(tm).unix())
    },

    // getOrder Book: (pair)=>{
    //     module.exports.log("got orderbook command")
    //     polon.exchange_methods.getOrderBook(pair,globals.maxOrderbookLength,function(err,data){
    //         if(err){
    //             module.exports.log(err)
    //             module.exports.getOrderBook(pair)   
    //         }else{
    //             // gsocket.emit('account.returnorderbook', data)
    //             module.exports.log("Sent orderbook")

    //             return data
    //         }
    //     })
    // },

    deBounceObjects: [],

    debounce: (name, bounceTime)=>{
        bounceTime = bounceTime || 250
        var debounce = false
        var msNow = new Date().getTime()
        var obj = me.deBounceObjects.find(item => item.name == name)
        if(obj){
            debounce = (obj.last + bounceTime > msNow)
        }else{
            obj={
                name: name,
                last: msNow
            }
            me.deBounceObjects.push(obj)
        }
        // functions.log("debounce interval: " + (msNow - obj.last))
        obj.last = msNow

        if(debounce){
            functions.log("*****Debounced :" + name)
        }
        return debounce
    },

    log: (msg, date, inUTC, sendToClient)=>{
        if(modules.globals.logMessages){
            if(inUTC){date = me.sqlDate(date)}
            if(!date){date = modules.moment().utcOffset('-0700').format('MMMM Do YYYY, h:mm:ss a')}
            console.log(date + " -- " + msg)
        }
        if(sendToClient){
            me.sendMessageToClient('console_log', date + " -- " + msg)
        }
    },
    logToFile: (data, filename, noFileDate)=>{
        var date = ""
        var now = new Date()
        var fileNow = now.toDateString().split(" ").join("_") + '.txt'
        var newline = String.fromCharCode(13, 10);
        if(!noFileDate){ date = "_" + modules.globals.moment().utcOffset('-0700').format('MMMM Do YYYY, h:mm:ss a')}
        var logfile = './logs/' + filename + date + '.txt'
        
        try {
            modules.fs.writeFileSync(logfile, newline +  date + "--" + data,function(err){
                modules.fs.close()
            } )
        }catch(e){ console.log(e) }
      
       
      
    },
    makeid: (l)=>{
    var text = "";
        var char_list = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for(var i=0; i < l; i++ )
        {  
            text += char_list.charAt(Math.floor(Math.random() * char_list.length));
        }
        return text;
    },
    sendMessageToClient: (msgName,msg,delayInSeconds)=>{
        delayInSeconds = delayInSeconds || 0
        delayInSeconds *= 1000
        
        setTimeout(function() {
            try {
                if(modules.globals.gsocket){
                    modules.globals.gsocket.emit(msgName, msg)
                }
            } catch (e) {}
        }, delayInSeconds);
        
    },
    saveUserConsole: (data)=>{
        var newline = String.fromCharCode(13, 10)
        modules.globals.account.console += newline + data
    },

    fixPoloLogMessage: (message)=>{
        return JSON.stringify(message).replace(/'/g, "").replace(/"/g, "").replace(/,/g, " ").replace(/{/g, "").replace(/}/g, "").replace(/[/\\*]/g, "")
    },

    jettyLog: (msg, row, clear)=>{
        if(modules.globals.debugMode){
            console.log(msg)
        }else{
            if(clear){modules.jetty.clear()}
            modules.jetty.moveTo([row,0])
            modules.jetty.text(msg)
        }
    },
    
    jettyClear: ()=>{
        // jetty.clear()
        console.log('\033[2J')
    },

    roughSizeOfObject: (object)=> {
        var objectList = [];
        var recurse = function( value ) {
            var bytes = 0;
    
            if ( typeof value === 'boolean' ) {
                bytes = 4;
            } else if ( typeof value === 'string' ) {
                bytes = value.length * 2;
            } else if ( typeof value === 'number' ) {
                bytes = 8;
            } else if (typeof value === 'object'
                    && objectList.indexOf( value ) === -1) {
                objectList[ objectList.length ] = value;
                for( i in value ) {
                    bytes+= 8; // assumed existence overhead
                    bytes+= recurse( value[i] )
                }
            }
            return bytes;
        }
            return recurse( object );
    }
}


