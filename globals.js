


// var debug = inDebugMode()




var me = module.exports = {

  moment:require('moment'),

  /** port and server config */
  HTTPport: 3000,
  serverPort: 3001,
  tradeServer: "http://localhost",  //running on same machine
  tradeServerConnectString: ()=>{
    return me.tradeServer + ":" + me.serverPort
  },

  pairSets: [ /**{pair:"BTC_XMR"} */ ],
  marketData: [ /**{pair:"BTC_ETH",spread: 0, orderBook:[],ticker:[] }*/],
  tradePairHistories: [],
  polo_moversData: [],
  binance_moversData: [],
  polo_candlesData: [],
  binance_candlesData: [],

  logMessages: true,
  gsocket: undefined,
  maxOrderbookLength: 20,

  initialStartupComplete: false,

/** Market Trade Data Constants */
  marketTradeHistoryLength: 1,      /**days length for initial market history grab */
  marketTradeHistoryMinLength: 1,   // min days history to grab
  marketTradeStartEndDiff: 1,       /**hours time diff for each slice to grab from server */
  aggregateSliceLength: 30,         /**seconds slice length */
  getTradesInterval: 400,           /**ms - eg: 200ms = 5 API calls per second, 250ms = 4 calls */
  getTradesMaxInterval: 210,        //use 170 after debugging
  cycleLengthTarget: 10,      //target length in seconds for each cycle of data collection
  getTradesSlowInterval: 400,
  getTradesMediumInterval: 300,
  
/** Buy / Sell constants */
  dumpCoinFactor: .6,               /** amount to reduce current price to ensure coin gets dumped - default 40% - */
  buyNowFactor: 1.05,
  maxBuyAmountBTC: .015,

  debugUserName: "Stefano",

  
  

  /** account data */
  account: {
    userName: "Stefano",
    holdings: [],
    orders: [],
    managedHoldings: [],
    margin: [],
    triggers: [],  //stop limits
    alarms: [],
    fees: [],
    console: ""
  },

  clientsConnected: 0,

  /** analysis constants */
  inMemoryAnalysisChunkSize: 2000,
  // analyse_TPriceValue: 4,
  DBDataLengthInWeeks: 1,

  /** startup Mode and commandline data */
  disableClientMode: false,   //act only as the DB population app, no client mode
  disableDBMode: true,       //stop querying POLO for data, enable by commandline
  orderManagerSimulationMode: false,
  orderManagerSimulationLearningMode: false,
  orderManagerSimulationRebuildData: false,

  orderManagerEnableLiveTrading: false,  //enable real live trading
  orderManagerEnableSpreader: false,
  // archiveDBToDisk: false,     //archive data on start, also disables DBmode above
  // doBackTesting: false,       //do the backtests of analysis functions on data in the db
  // backTestingCoin: null,        // command line param for the coin to test
  // backTestingStart: null,
  // backTestingEnd: null,
  simLearnTimePeriod: "",


/** debugging variables */
  debugMode: inDebugMode(),
  pairTestStartPoint: 2000,
  pairTestDidOutPutFIle: false,
  tradeCollectionConsoleOutput: true,
  disableSpeech: true,
  

  /** which pairs to stream trades data - comes from client */
  //tradeStreamPairs: "",  //["USDT_BTC", "BTC_ETH", etc...]
  orderBookPair: "USDT_BTC",  //default pair to stream orderbook data
  basePairLocked: true  //TODO 
}




function inDebugMode(){
  if(process.execArgv && process.execArgv[0]){
    if( process.execArgv[0].indexOf('--inspect') > -1){
      console.log("Debug Mode Detected")
      return true
    }
  }
}
