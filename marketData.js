


// var keys = require('./polo_keys.js');
// const Poloniex = require('poloniex-api-node');
// let poloniex = new Poloniex(keys.pkey, keys.psec, { socketTimeout: 15000 });

var request = require('request')  //request({url:http://xxx, json: true}, callack(err,res, json))

var globals = require('./globals')
var functions = require('./functions')

/** CoinMarketCap API data
 *   docs: https://coinmarketcap.com/api/
 *   endpoint: https://api.coinmarketcap.com/v1/global/
 *   endpoint: https://api.coinmarketcap.com/v1/ticker/?limit=10
 * Sample:
 ticker:
 {
		"id": "bitcoin", 
		"name": "Bitcoin", 
		"symbol": "BTC", 
		"rank": "1", 
		"price_usd": "573.137", 
		"price_btc": "1.0", 
		"24h_volume_usd": "72855700.0", 
		"market_cap_usd": "9080883500.0", 
		"available_supply": "15844176.0", 
		"total_supply": "15844176.0", 
		"percent_change_1h": "0.04", 
		"percent_change_24h": "-0.3", 
		"percent_change_7d": "-0.57", 
		"last_updated": "1472762067"
  }
  global:
  {
	"total_market_cap_usd": 12756692479.0, 
	"total_24h_volume_usd": 135078435.0, 
	"bitcoin_percentage_of_market_cap": 83.34, 
	"active_currencies": 653, 
	"active_assets": 59, 
	"active_markets": 1995
}						

 */


var me = module.exports = {

  data: {
    ticker_endpoint: "https://api.coinmarketcap.com/v1/ticker/?limit=10",
    global_endpoint: "https://api.coinmarketcap.com/v1/global/",
    global_data : {},
    ticker_data : [],
    values: {
      globalMarketCapHistory: [],
      BTCMarketCapHistory: [],
      ETHMarketCapHistory:[],

    }
  },

  getCoinMarketCap: ()=>{
    /** get major currency market cap number */
    request({url:me.data.ticker_endpoint, json: true}, function(err,res, json){
      try {
        me.data.ticker_data = json
        var BTCmk =  json.find(x=>x.id=="bitcoin").market_cap_usd
        if(BTCmk != me.data.values.BTCMarketCapHistory[0]){
          functions.pushToTopOfArrayWithLimit(me.data.values.BTCMarketCapHistory,BTCmk,20)
        }
        var ETHmk = json.find(x=>x.id=="ethereum").market_cap_usd
        if(ETHmk != me.data.values.ETHMarketCapHistory[0]){
          functions.pushToTopOfArrayWithLimit(me.data.values.ETHMarketCapHistory,ETHmk,20)
        }
      }catch(e){ console.log(e) }
    })
    /** get global market cap number */
    request({url:me.data.global_endpoint, json: true}, function(err,res, json){
      try {
        me.data.global_data = json
        functions.pushToTopOfArrayWithLimit(me.data.values.globalMarketCapHistory,json.total_market_cap_usd,20)
      }catch(e){ console.log(e) }
    })

  },
  updateMarketData: ()=>{
    /** first make sure all pairs exist */
    globals.pairSets.forEach(function(pair,index){
      var item = getitem(pair)
      if(!item){
        globals.marketData.push({
          pair:pair,
          spread: 0,
          orderBook: [],
          ticker: []
        })
      }
    })
    /** next get orderbooks and spread amounts*/
    // me.getSpreads()
  },

  
    
}

function getitem(pair){
  return globals.marketData.find(x=>x.pair == pair)
}
// function getOrderBooks(callback){
//   poloniex.returnOrderBook('all', 20, function(err,res){
//     if(!err){
//       callback(res)
//     }
//   })
// }