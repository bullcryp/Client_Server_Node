const Jetty = require("jetty")
const Poloniex = require('poloniex-api-node')
const keys = require('./polo_keys.js')

var me = module.exports = {
  //algo:       require('./algo'),
  //app:        require('./app'),
  //db:         require("./db/db"),
  //functions:  require('./functions'),
  //Jetty:      require("jetty"),
  //marketData: require("./marketData"),
  //om:         require("./orderManager"), 
  //polon:      require('./polo_functions'),
  //tradeReport:require('./tradeReport')

  _:         require('lodash'),
  async:      require('async'),
  bodyParser: require('body-parser'),
  fs:         require('fs'),
  globals:    require('./globals'),
  jetty:      new Jetty(process.stdout),
  moment:     require("moment"),
  // mysql:      require('mysql'),
  nStats:     require("./nodeStats"),
  os:         require('os'),
  poloniex:   new Poloniex(keys.pkey, keys.psec, { socketTimeout: 15000 }),
  request:    require('request'),
  say:        require('say'),
}

// var j = new Jetty(process.stdout)
// var keys = require('./polo_keys.js'),
// var Poloniex =  require('poloniex-api-node')
// var p = new Poloniex(keys.pkey, keys.psec, { socketTimeout: 15000 })

