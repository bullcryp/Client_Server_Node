var globals = require("./globals") 
var polon = require("./polo_functions")  
var algo = require('./algo')
var om = require('./orderManager')
var functions = require('./functions')
var db = require("./db/db")

var moment = require("moment")
var _ = require('lodash')
var fs = require('fs')



var me = module.exports = {
  backTestData: undefined,  //store the backtest data so we don't constantly reload it
  simMode: globals.runBacktestOrderManagerSimulation,
  simBestBTCLevel:0, //for the brute force stoplevels loop
  simBestMaxBTC: 0,
  simTmpSLLevel:.49,  //current sim learn SLL amount
  simCurrentSlls: undefined,  //current stop loss levels SET being worked on
  simLoopTmpBestLevel: 0,
  simHighestMaxGain: 0,   // highest BTC reached
  simLearnLoopsCount: 0,  //current number of loops
  simLearnLoops: 3,  //number of loops to complete SLL learning
  simLearnLoopDirectionRev: false,
  simSLPercentStep: .5,
  simSLStepMin: .5,
  simSLStepSize: .25,  //.1 TODO
  simSLStepMax: 5,
  simSLLStep: 0, //current sim learn SLL step

  eraseSLLs: false,
  eraseBVs: true,
  initLearningLoop: ()=>{
    
        if(!om.initDone){
          om.init(true)
        }
    
        //enable only tier1
        algo.simVariables.enableTier1 = true
        algo.simVariables.enableTier2 = false
        algo.simVariables.enableTier3 = false
        algo.simVariables.enableTier4 = false

        //set the start and end dates
        var myArgs = process.argv.slice(2)
        if(globals.simLearnTimePeriod != ""){
          if(isNaN(globals.simLearnTimePeriod.substring(0,1))){
            algo.simVariables.backtestDateStart = functions.sqlDate(  moment().subtract(1,globals.simLearnTimePeriod)  )
            algo.simVariables.backtestDateEnd = functions.sqlDate( moment() )
          }else{
            var length = parseInt(globals.simLearnTimePeriod.substring(0,1) )
            var period = globals.simLearnTimePeriod.substring(1)
            algo.simVariables.backtestDateStart = functions.sqlDate(  moment().subtract(length,period)  )
            algo.simVariables.backtestDateEnd = functions.sqlDate( moment() )
          }
        }else{
          algo.simVariables.backtestDateStart="2017-12-05 00:00:00"
          algo.simVariables.backtestDateEnd="2018-1-05 23:59:59"
        }

        globals.simLearnTimePeriod = algo.simVariables.backtestDateStart.substring(0,10) + "--" + algo.simVariables.backtestDateEnd.substring(0,10)

        console.log(globals.simLearnTimePeriod)

        algo.vars.enableStopLossModifier = true
    
        // set tier - for now. eventually do 2 or more tiers of autotune
        me.simCurrentTier = algo.omParams.tier1 //= {
          //stopLossLevels: [ {percent: -100, stopLoss: 2} ],
        //   tprice: 0,
        //   tbvol: 0,
        //   tslicesAbove: 0,
        //   tbvol_tsvol_ratio: 0,
        //   calmFactorBelow: 0,
        //   checkBuySignal: (result,holding)=>{
        //     var signal = false
        //     var t1 = algo.omParams.tier1
        //     if(result.t_price.t >= t1.tprice){
        //       if(result.t_bvol.t > t1.tbvol){
        //         if(result.dv_lchunk <= t1.calmFactorBelow){
        //           if(result.t_svol.t < result.t_bvol.t/t1.tbvol_tsvol_ratio){
        //             if(algo.validateSlices(result.slicesAboveBVolMean, t1.tslicesAbove)){
        //               signal = 1.1
        //             }
        //           }
        //         }
        //       }
        //     }
        //     return signal
        //   }
        // }
        me.simCurrentTierValue = 1.1
        me.simCurrentSlls = me.simCurrentTier.stopLossLevels //= []

        // initialize test SLL set
        if(me.eraseSLLs){
          setTestStartingSLL(-1,1)    // set all levels to 1 to start
          function setTestStartingSLL(p,s){
            //if(noPrev == undefined){noPrev=false}
            setSLLevel(p,s)
          }
        }

        me.buyvariables = [
          { name: 'tprice',value: 3, bestValue: 3, min:3, max: 10, step: .1,fineStep:.01,fineRange:.09,d:2 },
          { name: 'tbvol',value: 3, bestValue: 3, min:3, max: 10, step: .1,fineStep:.01,fineRange:.09,d:2 },
          { name: 'tbtrades',value: 0, bestValue: 0, min:0, max: 10, step: .2,fineStep:.09,fineRange:.09,d:2 },
          { name: 'tbvol_tsvol_ratio',value: 1, bestValue: 1, min:.05, max: 1, step: .05,fineStep:.01,fineRange:.04,d:2 },
          { name: 'tslicesAbove',value: 2, bestValue: 2, min:2, max: 10, step: 1,d:0},
          { name: 'calmFactorBelow',value: .02, bestValue: .002, min:.002, max: .02, step: .00096,fineStep:.00001,fineRange:.00019,d:5 },
          { name: 'slModStartAfter',value: 110, bestValue: 110, min:0, max: 1000, step: 50,fineStep:10,fineRange:40,d:0 },
          { name: 'slModMaxMinutes',value: 12, bestValue: 12, min:2, max: 40, step: 2,fineStep:1,fineRange:1,d:0 },
        ]
        
        setBuyVariables(me.eraseBVs)

        //get total rows in backtest table
        db.getBacktestTotalRows(algo.simVariables.backtestDaysStart, algo.simVariables.backtestDaysEnd,
          algo.simVariables.backtestDateStart, algo.simVariables.backtestDateEnd,
          function (res) {
            console.log("Trade Rows Count: " + res)
            var totalRows = res
            var groups = parseInt(totalRows / algo.simVariables.backtestBlockLoadSize)

            // algo.simVariables.backtestBlockLoadSize = totalRows + 1
            getBackTestData(totalRows,groups,function () {
              startLoops()
            })
        })

        //do the SLL learning Loops
        
        function startLoops(){

          //doCoinsLearningLoop()
          //loop to train the StopLoss Levels - first run with buy variables wide open - lots of trades
          //doSllLearningLoop(1)
          //loop to train the Buy Entry Levels - 'true' to set initial values
          doBuyEntryLearningLoops(1)
          doSllLearningLoop(1)

          /** overwrite the initial buy variables again */
          setBuyVariables(true,true)

          //repeat
          doBuyEntryLearningLoops(1,true)
          doSllLearningLoop(1)

          doBuyEntryLearningLoops(2,true)
          doSllLearningLoop(2)
          
/** log everything to files */
          var newline = String.fromCharCode(13, 10)

          var str = JSON.stringify(me.buyvariables).replaceAll('"','').replaceAll("{",newline+"{") + newline + newline
          var stats = newline + newline + "Erase BV: " + me.eraseBVs + newline + "Erase SLL: " + me.eraseSLLs + newline 
          stats += newline + "endBTC: " + om.accountObject.simResult.endBTC + newline +
          "   bestEndBTC: " + me.simBestBTCLevel + newline +
          "   maxBTC: " + om.accountObject.simResult.maxBTCHeld + newline +
          "   largestMaxGain: " + me.simHighestMaxGain
            console.log( str + stats )

            functions.logToFile(str + stats,"buyvariables_" + functions.makeid(4) + "_" + globals.simLearnTimePeriod)
          
          str = JSON.stringify(me.simCurrentSlls).replaceAll('"','').replaceAll("{",newline+"{")
            console.log(str)
            functions.logToFile(str + stats,"stoplosslevels_" + functions.makeid(4) + "_" + globals.simLearnTimePeriod)
          
/** insert a sim result into the db */

          /** and.... terminate */
          process.exit()

        }
    
        
            //end loop content
          
  }
}

function setBuyVariables(overWrite, minValue){
  var t = me.simCurrentTier
  var v = me.buyvariables

  if(!overWrite){
    setValue('tprice',t.tprice) 
    setValue('tbvol',t.tbvol)  
    setValue('tbtrades',t.tbtrades)
    setValue('tbvol_tsvol_ratio',t.tbvol_tsvol_ratio)
    setValue('tslicesAbove',t.tslicesAbove) 
    setValue('calmFactorBelow',t.calmFactorBelow)
    setValue('slModStartAfter',algo.vars.slModStartAfter)
    setValue('slModMaxMinutes',algo.vars.slModMaxMinutes)
  }else{
    t.tprice = getValue('tprice') 
    t.tbvol = getValue('tbvol')  
    t.tbtrades = getValue('tbtrades')
    t.tbvol_tsvol_ratio = getValue('tbvol_tsvol_ratio')
    t.tslicesAbove = getValue('tslicesAbove') 
    t.calmFactorBelow = getValue('calmFactorBelow')
    algo.vars.slModStartAfter = getValue('slModStartAfter')
    algo.vars.slModMaxMinutes = getValue('slModMaxMinutes')
  }
    function getValue(name,minValue){
      var v = me.buyvariables.find(x=>x.name==name)
      var tmp = v.value
      return functions.round(tmp,v.d)
    }
    function setValue(name,val){
      var v = me.buyvariables.find(x=>x.name==name)
      v.value = minValue?v.bestValue:val
      v.bestValue = v.value
    }
}

/** Loop Functions for Sim and Backtest */
function doBuyEntryLearningLoops(n,fineTune, setDefaults,slMod){

  var foundEnd, foundTieHigh,didFineTune,prevStep,prevMax,maxTrades
  
  var prevBVSet = ""

  if(setDefaults){
    var bv = me.buyvariables
    bv[0].value = 5
    bv[1].value = 5
    bv[2].value = .5
    bv[3].value = 3
    bv[4].value = .0068
    bv[5].value = 110
    bv[6].value = 12
  }
  var loops = (n || me.simLearnLoops)
  for(var i =1;i <= loops; i++){
    
    var tmp = JSON.stringify(me.buyvariables)
    if (tmp == prevBVSet){
      break
    }else{
      prevBVSet = tmp
    }
    me.simLearnLoopsCount = i
//for every setting in buyvariables
    for( v of me.buyvariables){
      if(slMod){
        if(!v.name.includes('slMod')){
          continue
        }
      }else if(!algo.vars.enableStopLossModifier){
        if(v.name.includes('slMod')){
          continue
        }
      }
//set current step to min starting value
      var origValue = v.value
      v.value = v.min 
      setBuyVariables(true)
      foundEnd = false
      foundTieHigh = false
      didFineTune = false
      prevStep = undefined
      prevMax = undefined
      maxTrades = undefined
//loop thru each possible value for that setting
      do {
//run backtest with current .buyVariables
        runBackTestLoop()
//store result and get newHigh result
        
        var result = analyseLoopResult(v.value)
        if(!maxTrades){
          maxTrades = result.trades
        }else if(result.trades < (maxTrades /1.5) && result.endBTC < om.accountObject.startBTC){
          //never let a loop reduce the trades found by more than half, if the BTC value is less than start value.
          //(if BTC value is less than start, a trades value of 0 would have a better value.)
          foundEnd = true
        }else if(result.trades > maxTrades){
          maxTrades = result.trades
        }
        if(result.newHigh){
          v.bestValue = v.value
          foundTieHigh = false
        }
        if(!foundTieHigh && result.tieHigh){
          v.bestValue = v.value
          foundTieHigh = true
        }

        console.log( (result.newHigh?"*****    ":"") + "Setting: " + v.name + "  value:"+v.value + "  Trades: " + result.trades + "    endBTC: " + result.endBTC + "   loop: "+ i + " of " + (n || me.simLearnLoops) + (result.newHigh?"*****":""))
        if(result.newHigh){console.log(" ")}

//check if we are at end of steps for this value
        if( (v.value + v.step) > v.max || foundEnd){
//set current level to best performing level
          v.value = v.bestValue
          console.log("Best value: " + v.bestValue)
//do fine tune loop for steps which have it
          if( i > 1 && !didFineTune && v.fineStep){
            console.log("Fine tuning " + v.name + "...")
/**reset the value, step size and max values of v to fine tune around it 
 * now the tuning will continue with the fine range around the best 
 * found value
*/    
            prevStep = v.step  //store the original values
            prevMax = v.max
            v.step = v.fineStep
            v.max = functions.round(v.value + v.fineRange,v.d)
            v.value = functions.round(v.value - v.fineRange, v.d)
            didFineTune = true
          }else{
            foundEnd = true
            v.step = prevStep || v.step //restore the original values
            v.max = prevMax || v.max
          }
        }else{
//increment the step
            v.value = functions.round(v.value + v.step,v.d)
        }

//write the values for the next test
        setBuyVariables(true)

      } while (!foundEnd);
      
    }

  }

  return false
}

function doCoinsLearningLoop(){

    /* run baseline loop - all coins */
    runBackTestLoop()
    var result = analyseLoopResult(sl)

    //loop thru each coin and test disabling it
    globals.pairSets.forEach(function(coin){
      algo.omParams.excludeIf.push({value:coin, present:true})
    })
}

function doSllLearningLoop(n){

  var foundEnd, foundTieHigh,prevStep,prevMax, bestLevel

  prevSLLSet = ""
  var loops = (n || me.simLearnLoops)

  for(var i =1;i <= loops; i++){
    var tmp = JSON.stringify(me.simCurrentSlls)
    if (tmp == prevSLLSet){
      break
    }else{
      prevSLLSet = tmp
    }
    me.simLearnLoopsCount = i
//for every percent step in stoploss set
    for(p = 0; p <= 20; p+=me.simSLPercentStep){
//store current stoploss level in case no better new level is found
      if (me.simHighestMaxGain < p){
        break;
      }
      bestLevel = getSLLevel(p)
      foundTieHigh = false
      me.simSLLStep = p
      //for each stop in stoploss levels
      for(sl = me.simSLStepMin; sl <= me.simSLStepMax; sl += me.simSLStepSize){

//set current SLL 
        setSLLevel(p,sl)
//run backtest with current .buyVariables
        runBackTestLoop()
//store result and get newHigh result
        var result = analyseLoopResult(sl,true)
        if(result.newHigh){
          bestLevel = sl 
          foundTieHigh = false
        }
        if(!foundTieHigh && result.tieHigh){
          bestLevel = sl 
          foundTieHigh = true 
        }

        console.log( (result.newHigh?"*****    ":"") + "Percent Step: " + p + "  stoploss: " + sl + "  Trades: " + result.trades + "    endBTC: " + result.endBTC + "   loop: "+ i + " of " + (n || me.simLearnLoops) + (result.newHigh || result.tieHigh?"*****":""))
        if(result.newHigh){console.log(" ")}

        
      }
      console.log("Best value: " + bestLevel)
      setSLLevel(p,bestLevel)
    }
  }
  
  
  // if(sl > 5){
  //   //set current levels to BEST stoploss level
  //   me.simCurrentSlls = me.simCurrentTier.stopLossLevels = JSON.parse(me.simBestSLLSet) 
  //   //finalize SL set
  //   setSLLevel(me.simSLLStep,me.simLoopTmpBestLevel,true)
  //   //increment half a percent
  //   me.simSLLStep += .5  //(me.simLearnLoopDirectionRev==false)?.5:-.5
    
  //   //remove the new default stop from the set until tested
  //   var index = me.simCurrentSlls.findIndex(x=>x.percent == me.simSLLStep)
  //   me.simCurrentSlls.splice(index,1)
  //   me.simBestSLLSet = JSON.stringify(me.simCurrentSlls)

  //   me.simTmpSLLevel = .49
  //   me.simLoopTmpBestLevel = 0
    
    
  //   if(me.simSLLStep <=20 && me.simSLLStep >=0){
      
  //     if (me.simHighestMaxGain < me.simSLLStep){
  //       /**we don't need to continue, 
  //        * no holding had a gain higher
  //        * than this level */
  //       exitLoop()
  //       return true
  //     }
  //     //and add the new row
  //     setSLLevel(me.simSLLStep,.5)
      
  //   }else{
  //     // pc over 20, we are done
  //     exitLoop()
  //     return true
  //   }
  // }else{
  //   setSLLevel(me.simSLLStep,sl)
  // }
  // function exitLoop(){
  //   console.log("BTC: " + me.simBestBTCLevel)
  //   console.log("MAX: " + me.simBestMaxBTC)
  //   me.simSLLStep = 0 //?
  //   me.simLearnLoopsCount += 1
  //   //me.simLearnLoopDirectionRev = !me.simLearnLoopDirectionRev
  //   var tmp = getSLLevel(undefined,true)
  // }
  return false
}
function analyseLoopResult(v,forStopLoss){
  var noTransactions = false
  var newHigh = false
  var tieHigh = false
  var trades = 0
  try {
    trades = om.managedHoldingsHistory.length
    var maxGain = om.managedHoldingsHistory.reduce(function(l,e){
      return e.maxGain > l.maxGain ? e : l
    }).maxGain
  } catch (error) {noTransactions = true}  //if wee err here, array is empty = no trades
    if(maxGain > me.simHighestMaxGain){
      me.simHighestMaxGain = maxGain
    }
    

    om.managedHoldingsHistory = []
    om.accountObject.transactions = []
    om.accountObject._BTCAvailable = algo.simVariables.startBTC
    om.accountObject.startBTC = algo.simVariables.startBTC
    om.managedHoldings = []

    if(!noTransactions){
      if(me.simBestBTCLevel < om.accountObject.simResult.endBTC){
        newHigh = true
        me.simBestBTCLevel = om.accountObject.simResult.endBTC
        if(forStopLoss){ me.simTmpSLLevel = functions.round(v,2) }
        if(forStopLoss){
          me.simBestSLLSet = JSON.stringify(me.simCurrentSlls)  //.replaceAll("percent","p").replaceAll("stopLoss","sl").replaceAll("prevStopLoss","psl")
          me.simBestSLLSetFinal = JSON.stringify(me.simCurrentSlls.filter(x=>x.percent <= (me.simSLLStep + .5) ))
          //console.log("     *****" + JSON.stringify(JSON.parse(me.simBestSLLSet).filter(x=>x.percent <= me.simSLLStep)).replaceAll("percent","p").replaceAll("stopLoss","sl").replaceAll("prevStopLoss","psl") + "   BTC:" + me.simBestBTCLevel)
        }else{
          // me.simBestBLSet =
          // me.simBestBLSetFinal =
        }
        me.simLoopTmpBestLevel = 0
      }else if(me.simBestBTCLevel == om.accountObject.simResult.endBTC && me.simLoopTmpBestLevel == 0){
        me.simLoopTmpBestLevel = v
      }
      if(me.simBestBTCLevel <= om.accountObject.simResult.endBTC){
        tieHigh = true
      }
      if(me.simBestMaxBTC < om.accountObject.simResult.maxBTCHeld){
        me.simBestMaxBTC = om.accountObject.simResult.maxBTCHeld
      }
    }
      return {newHigh:newHigh, tieHigh:tieHigh, trades:trades, endBTC:om.accountObject.simResult.endBTC}
}

function getSLLevel(sll,reset){
  var levels = me.simCurrentSlls
  if(sll == 0){sll = -100}
  var index = levels.findIndex(x=>x.percent == sll)
  if(index >=0){
    if(reset){
      levels[index].stopLoss = .5
    }
    
    return levels[index].stopLoss
  }else{
    return 0
  }
  
}
function setSLLevel(percent,sl, final){
  var levels = me.simCurrentSlls
  if(percent==0){percent = -100}
  var index = levels.findIndex(x=>x.percent == percent)
  if(percent == -1){
    //set all levels to a starting value
    me.simCurrentSlls = levels = []
    for(i=0;i<=20;i+=.5){
      var p = i; if (p==0){p=-100}
      levels.push({percent:p,stopLoss:sl})
    }
    // levels[0].stopLoss = .5
    // levels[40].stopLoss = 3
    me.simBestSLLSet = JSON.stringify(levels)
  }else if(final){
      if(sl > 0){
        if(index < 0){
          levels.push( {percent: percent, stopLoss: functions.round(sl,2)} )
          levels.sort( function(a,b){ return a.percent - b.percent} )
        }else{
          levels[index].stopLoss = functions.round(sl,2)
        }
        
      }
      if(index < 0){
        index = levels.findIndex(x=>x.percent == percent)
      }
      if(index > 0 && levels[index].stopLoss == levels[index-1].stopLoss){
        levels.splice(index,1)
      }
      me.simBestSLLSet = JSON.stringify(me.simCurrentSlls) //.replaceAll("percent","p").replaceAll("stopLoss","sl").replaceAll("prevStopLoss","psl")
      //me.simBestSLLSetFinal = JSON.stringify(me.simCurrentSlls.filter(x=>x.percent <= (me.simSLLStep + .5) ))
  }else{
    
    if(index >=0){
      levels[index].stopLoss = functions.round(sl,2)
      /** if(final){
        //   if(sl != .49){
        //     levels[index].prevStopLoss =  functions.round(sl,2)
        //   }else{
        //     if(levels[index].prevStopLoss){
        //       levels[index].stopLoss = levels[index].prevStopLoss
        //     }else{
        //       if(index > 0){
        //         levels.splice(index,1)
        //       }
        //     }
        //   }
        //   try{
        //     if(index > 0 && levels[index].stopLoss == levels[index-1].stopLoss){
        //       levels.splice(index,1)
        //     }
        //   }catch(e){}
        // }
        
        // if( (sl==.49 && index > 0) || (final && index > 0)  ){
        //   if( sl==.49 || (levels[index].stopLoss == levels[index-1].stopLoss) ){
        //     levels.splice(index,1)
        //   }
       }*/
    }else{
      var lvl = {percent: percent, stopLoss: sl}
      levels.push(lvl)
      levels.sort( function(a,b){ return a.percent - b.percent} )
    }
  }
  var log
  log = JSON.stringify(levels.filter(x=>x.percent <= Math.max(percent,0) )).replaceAll("percent","p").replaceAll("stopLoss","sl").replaceAll("prevStopLoss","psl")
  if(final){log += " - F"}
  console.log (log)
}
function runBackTestLoop(save){
  
  //console.log("Getting backtest Group")
  //load the rows in groups of 10,000, and send them in sequence to simulate real trades
  functions.timeEvent(true)

  var groups = me.backTestData.length

  for(var group = 0; group < groups; group++){
    if(me.backTestData[group].length > 0){
      for(item of me.backTestData[group]){
        om.analyseCryptoReport(item,true)
      }  
      //console.log(group)
    
    }
  }
  //sell any remaining holdings
  for(holding of om.managedHoldings){
    om.dumpCoin(holding)
  }

  var gainloss = 0
  var maxGain = 0
  om.accountObject.transactions.forEach(function(item){
    gainloss += item.gainLoss
    maxGain += item.maxGain
  })

  om.managedHoldingsHistory.sort(function(a,b){ return new Date(a.buyDate) - new Date(b.buyDate)})
  
  om.accountObject.simResult = {
    startBTC: algo.simVariables.startBTC,
    endBTC: om.accountObject.getTotalAccountBTCValue(),
    minBTCHeld: om.accountObject.minBTCHeld,
    maxBTCHeld: om.accountObject.maxBTCHeld,
    maxBTCDate: om.accountObject.maxBTCDate,
    maxholdings: algo.simVariables.vMaxHoldings,
    totalGain: gainloss, 
    totalMaxGain: maxGain,
    variables: algo.omParams,
    simVariables: algo.simVariables,
    transactions: om.managedHoldingsHistory,
    log: om.accountObject.simLog
  }
  
  console.log(
    "endBTC: " + om.accountObject.simResult.endBTC + 
  "   bestEndBTC: " + me.simBestBTCLevel + 
  "   maxBTC: " + om.accountObject.simResult.maxBTCHeld + 
  "   largestMaxGain: " + me.simHighestMaxGain +
  "   Current Loop: " + me.simLearnLoopsCount 
  )
  
  functions.timeEvent(false,"Loop Time: ")

  /** save to db, if save == true */
  if(save){
    db.saveBacktestSimResult(om.accountObject.simResult, function(err,res){
      if(err){
        console.log(err)
      }else{
        console.log("Sim saved to DB")
      }
    })
  }
  
  /** TODO - check bitcoin status if going up or down
   * don't buy coins when bitcoin increasing
   */

  
  
}
function getBackTestData(totalRows, groups, callback){
  
  if(!me.backTestData){

    me.backTestData = new Array(groups)
    console.log("Total groups to load: " + (groups + 1) )
    console.log("Loading group: 1 from DB" )

    for( var group = 0; group <= groups; group++){
      // me.backTestData[group] = new Array[algo.simVariables.backtestBlockLoadSize]
      var start = group * algo.simVariables.backtestBlockLoadSize
      var count = algo.simVariables.backtestBlockLoadSize

      
      db.getBacktestRows(start, count,algo.simVariables.backtestDaysStart,algo.simVariables.backtestDaysEnd,
        algo.simVariables.backtestDateStart,algo.simVariables.backtestDateEnd,
        group, function(res,group){
          
          if(res.length > 0){
            me.backTestData[group] = res
            if(group == groups){
              console.log("Finished loading groups")
              callback()
            }else{
              console.log("Loading group " + (group + 2) + " from DB")
            }

          }
      })
    }
    var tmp = 0

  }else{
    callback()
  }

    
  
}
