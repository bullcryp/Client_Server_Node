


var globals = require('./globals')
var functions = require('./functions')

const binance = require('node-binance-api');
const poloniex = require('poloniex-api-node')

var moment = require('moment')

var request = require('xhr-request')

// var db = require("./db/db")
var fs = require('fs')

if(!functions.isLocal() ){
var redis = require("redis")
var redisClient = redis.createClient()
// var redisBatch = redisClient.Batch()
}

/** variables for timer interval function polo_intervals
 * which starts grabbing Balances, Orders, and Margin Account
 * */
var masterInterval;
var MasterIntervalTime = 1000  //timer frequency to check sub-objects if it's time to execute them
var timeSpacing = 500  //how much time between calls


/** our exportable exchange_methods*/
var me = exchange_methods = {

    intFunctions: [],  //array of functions to be executed at intervals

    /** helper function to reset next interval time for stored functions */
    setIntFunctionNextInterval(name, ms){
        me.intFunctions.forEach((execObject, index) => {
            if(execObject.name == name){
                var msNow = new Date().getTime() 
                execObject.last = msNow + execObject.interval + ms
                return
            }
        })
    },
    
    /** create and start the array of interval function calls */
    startAccountInfoIntervals: () => {

        var getBinanceCandles = me.getBinanceCandles
        var getPoloCandles = me.getPoloCandles
        

        var msNow = new Date().getTime() 
        var poloStartIn = msNow - 59000  //59 second offset (start in one second)
        var binanceStartIn = msNow-30000  //30 second offset from msNow
        /** 24 hour interval - clean out old backtest data */
        // me.intFunctions.push({ name: "cleanBackTest", object: ()=>{ cleanBacktest() }, interval: 86400, last: msNow - 80000000}) //Daily
        // me.intFunctions.push({ name: "getTicker", object: ()=>{ getTicker() }, interval: 1.5, last: false}) // 1 minutes

        // me.intFunctions.push({ name: "buildMoversData", object: () => { buildMoversData() }, interval: 60, last: msNow}) //********** buildMoversData (1 minute interval) */
        me.intFunctions.push({ name: "getPoloCandles", object: () => { getPoloCandles() }, interval: 60, last: poloStartIn}) //********** build Polo MoversData (1 minute interval) */
        me.intFunctions.push({ name: "getBinanceCandles", object: () => { getBinanceCandles() }, interval: 60, last: binanceStartIn}) //********** build Binance MoversData (1 minute interval) */


        var startIntFunction = () => {
            masterInterval = setInterval(() => {  //1 second interval master function
                var delay = 0
                me.intFunctions.forEach((execObject, index) => {
                    var msNow = new Date().getTime()                                                //get current time in ms
                    if (!execObject.last || execObject.last + (execObject.interval * 1000) < msNow) { // check if last + interval < now
                        execObject.last = msNow                                                     //set object last to current time
                        delay = (index + 1) * timeSpacing                                             //set start delay for spacing calls
                        setTimeout(execObject.object, delay)                                        //set an interval to run the function object
                    }
                })
            }, MasterIntervalTime)
        }
        startIntFunction()

        // me.getPoloCandles()

    }, // end startAccountInfoIntervals

    


    poloCandlesInProcess: false,
    getPoloCandles: ()=>{
        if(me.poloCandlesInProcess){
            return
        }
        me.poloCandlesInProcess = true
        var async = require('async')
        var pairs = []
        var start = moment.utc(Date.now() ).subtract(24,"hour").unix()
        var tasks = []
        var poloTradeCandles = []
        // var newMoversData = []
        functions.timeEvent(true)
        
        function qItemCallback(data,err){
            if(!data){return}
            poloTradeCandles.push(data)
            console.log("processed task: " + poloTradeCandles.length + "  pair:" + data.pair)
            if(poloTradeCandles.length >= pairs.length){
                functions.timeEvent(false,"PoloData Time: ")
                console.log("Got Candle Data")
                me.poloCandlesInProcess = false
                //all pair histories gotten
                //globals.tradePairHistories = tradePairHistories
                me.buildMoversData(poloTradeCandles, function(poloMoversData){
                    if(!functions.isLocal() ){
                        redisClient.set("polo_movers",JSON.stringify(poloMoversData))
                    }
                })
            }
        }

        var q = async.queue(function(task, callback){
            function timeFunc(task,callback){
                    request("https://poloniex.com/public?command=returnChartData&currencyPair=" + task.pair + "&start=" + start + "&end=9999999999&period=300",{json:true},function(err,data){
                        if(err !=null || typeof data != "object"){
                            q.push(task, qItemCallback)
                            callback()
                        }else{
                            callback({pair: task.pair,trades: data.reverse()})
                        }
                    })
            }
            var timeoutWrapper = async.timeout(timeFunc, 10000, task)
            timeoutWrapper(task, function(data,task){
                /** data.info == {task} */
                if(!data.trades || data.code == "ETIMEDOUT"){
                    q.push(data.info, qItemCallback)
                    console.log("Process task TimedOut: re-pushed task for pair: " + data.info.pair)
                    callback()
                }else{
                    callback(data)
                }
            } )
        },24)

        q.drain = function() {
            //at this point all the data is in tradePairhistories
            console.log("DRAINED")
        };
        
        request("https://poloniex.com/public?command=returnTicker",{json: true},function(err,data){
            //data = Object.keys(data).map(function (key) { return data[key]; });
            for (pair in data){
                pairs.push(pair)
            }
            
            //create task for each pair
            for (pair of pairs){
                var task = {
                    pair: pair,
                    start: start
                }
                tasks.push(task)
            }
            //queue up the array of tasks
            q.push(tasks,qItemCallback)
            
        })

        
        
    },

    getBinanceCandles: ()=>{
        var data
    },

    buildMoversData: (tradeCandles, callback)=>{
        try {

            var newMoversData = []
            for(pair of tradeCandles){
                var moverSet
                moverSet = getCoinChange(pair)
                newMoversData.push(moverSet)
            }
            // globals.polo_moversData = newMoversData
            
            callback(newMoversData)


        }catch(e){ console.log(e) }
        // return globals.moversData
        function getCoinChange(candles){
            //create pairChangeObject, and set the newest price and date vars
            var pairChangeArr = []
            
            var newestPrice = candles.trades[0].close
            var newestDate = candles.trades[0].date
            var age, oldestPrice,percentChange,cont
            var seconds = 300
            var count = 1
    
            if(candles.trades.length > 279){
                var candleStops = [1,3,6,24,48,96,144,candles.trades.length-1]
    
                for (candleStop of candleStops){
                    pairChangeArr.push( functions.round( functions.getPercentIncrease(candles.trades[candleStop].close,newestPrice),3) )
                }
                pairChangeArr = functions.mergeArrays([candles.pair],pairChangeArr.reverse() )
                return pairChangeArr
    
            }else{
                for(trade of candles.trades){
                    if(cont){continue}
                    count ++
                    age = functions.getTimeElapsed(newestDate,"second",trade.date)
                    if (age >= seconds  || count > candles.trades.length){
                        
                        oldestPrice = trade.close
                        percentChange = functions.round( functions.getPercentIncrease(oldestPrice,newestPrice),3)
                        //pairChangeArr.push(percentChange)
                        seconds = getNextStop(seconds,age,percentChange,pairChangeArr)
        
                        if(!seconds){
                            cont = true
                        }
                    }
                }
                pairChangeArr = functions.mergeArrays([candles.pair],pairChangeArr.reverse() )
                return pairChangeArr
            }
    
            
    
            function getNextStop(curStop,age,percentChange,pairChangeArr){
                // pairChangeArr.push(percentChange)
                var nextStop
                switch(curStop){
                    case 300:  /** 5 minutes */
                        nextStop = 900
                        if(age < nextStop){
                            pairChangeArr.push(percentChange)
                            break
                        }else{
                            pairChangeArr.push(0)
                        }
                    case 900:  /** 15 minutes */
                        nextStop = 1800
                        if(age < nextStop){
                            pairChangeArr.push(percentChange)
                            break
                        }else{
                            pairChangeArr.push(0)
                        }
                    case 1800:  /** 30 minutes */
                        nextStop = 7200
                        if(age < nextStop){
                            pairChangeArr.push(percentChange)
                            break
                        }else{
                            pairChangeArr.push(0)
                        }
                    case 7200:  /** 2 hours */
                        nextStop = 14400
                        if(age < nextStop){
                            pairChangeArr.push(percentChange)
                            break
                        }else{
                            pairChangeArr.push(0)
                        }
                    case 14400:  /** 4 hours */
                        nextStop = 28800
                        if(age < nextStop){
                            pairChangeArr.push(percentChange)
                            break
                        }else{
                            pairChangeArr.push(0)
                        }
                    case 28800:  /** 8 hours */
                        nextStop = 43200
                        if(age < nextStop){
                            pairChangeArr.push(percentChange)
                            break
                        }else{
                            pairChangeArr.push(0)
                        }
                    case 43200:  /** 12 hours */
                        nextStop = 86400
                        if(age < nextStop){
                            pairChangeArr.push(percentChange)
                            break
                        }else{
                            pairChangeArr.push(0)
                        }
                    case 86400:  /* 24 hours */
                        pairChangeArr.push(percentChange)
                        nextStop = false
                }
                return nextStop
            }
        }
    },
    
}  //end exportable polo methods


exports.exchange_methods = exchange_methods;



