
module.exports = {
  pumps: [
            {
              pair: "BTC_NEOS",
              start: "2017-05-03T14:55:00Z",
              end: "2017-05-03T16:55:00Z",
            },
            {
              pair: "BTC_GRC",
              start: "2017-05-10T17:00:00Z",
              end: "2017-05-10T18:15:00Z",
            },
            {
              pair: "BTC_PPC",
              start: "2017-05-03T10:15:28.000Z",
              end: "2017-05-03T13:42:34.000Z",
            }
          ]
}