// var clients = 0;


var polon =     require('./polo_functions.js')
var functions = require('./functions')
var globals =   require('./globals')
var app =       require('./app')
// var db =        require("./db/db")
var trades =    require("./trades")




/*** server to trades server receive trades */
app.clientSocket.on('trades', function(data,callback){
    // functions.log(data)
    callback(true)  //callback confirms to the server we got the trades
    trades.receiveTrades(data)
})

/*** client to server connection(s) */
//Whenever someone connects this gets executed
    app.clientSocket.on('connection', function(socket){

        globals.clientsConnected += 1

        globals.gsocket = socket


    /**********Client Connect / Disconnect Events */
        functions.log('Connected to trade server');
     
    //Whenever someone disconnects this piece of code executed
        socket.on('disconnect', function () {  
            functions.log('A user disconnected');
            globals.clientsConnected -= 1
            //cancel 1 second interval on disconnect
            //polon.exchange_methods.clearIntervals();
        });
/********* End Client Connect / Disconnect Events */


/***** server requests  */
        
        /** array of pairs that server will stream aggregate data to */

        /********************** end userdata */

/******* Other messages */


    });  //end socket.io on




