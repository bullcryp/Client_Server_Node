

var globals = require('./globals')
var functions = require('./functions')
var polon = require('./polo_functions')
var db = require("./db/db")
var om = require("./orderManager")


var me = module.exports = {
  // min spread size to enter into spread trades
  minSpread: .35,
  // minimum volume for pair to enter into spread trades
  minVolume: 75,
  //max number of spreads to play
  maxSpreads: 1,
  // size of trade(s) to maintain on spreads
  spreadUSDsize: 1,  // amount to buy, in USD
  spreadUSDrebuy: .1,  //amount to trigger another buy order
  
  spreadSat: 1, // 1 - how many sat over/under high bid, low ask
  // how many spread checks to keep to calculate spread average over time
  spreadAverageArrayLength: 75, //100 
  spreadCount: 0, //tmp for debugging

  didCreateSpreadPairs: false, //tmp holding of order details for purchases

  // current pairs eligible for spread
  spreadPairs: [],

  spreadOrders: [ /**{ pair:"pair", orderIds:[] }*/ ],
  //spreadHoldings: [],

  sat: (num)=>{
    return num * .00000001
  },

  // managedHoldings: globals.account.managedHoldings,
  // holdings: globals.account.holdings,

  doSpreads: ()=>{
    /** MAIN LOOP */

    /** bail if we don't have any histories - start up*/
    if(globals.tradePairHistories.length < globals.pairSets.length-5){return}
    //  if(!globals.orderManagerEnableLiveTrading){return} //TODO enable this

    //add all potential spread pairs to the spread pairs
    if(!me.didCreateSpreadPairs){
      me.createSpreadPairs()
    }
    //check all pairs current spreads,add to and maintain the eligibleSpreads array
    //me.addorUpdateManagedSpreads()

    // maintain all current spread orders, get back number of 
    // spreads that are buying
    var spreadsWithBuyOrders = me.checkAndUpdateOrders()

    /** check pread pairs to see if we are 
     * currently holding or have orders, and if
     * not, place those orders, if elligible*/
    for(var spread of me.spreadPairs){
       
      var eligible = spread.isEligible()
      var isManaged = me.isManagedSpread(spread)
      var hasBuyOrders = spread.orders.buy > 0
      var buyAmount = spread.orders.buy
      var hasSellOrders = spread.orders.sell > 0
      var sellAmount = spread.orders.sell
      var currentSpreadBuys = 0
      if (!eligible && !hasBuyOrders){
        spread.buyingStarted = false
      }
        
        /**if not in managed holdings */  
        if(eligible ){ // if eligible
          if(currentSpreadBuys < me.maxSpreads){
            if( (!hasBuyOrders && (spreadsWithBuyOrders < me.maxSpreads) ) ||
                  (buyAmount <= me.spreadUSDrebuy && spread.buyingStarted) ){
                // buy spread, and create managed spread holding
                me.placeSpreadBuyOrder(spread)
                currentSpreadBuys += 1
            }
          }
        }else{ 
          //if no longer elligible, cancel remaining buy orders
          if(buyAmount > 0){
            me.cancelSpreadBuyOrders(spread)
          }
          //not elligible, no sell orders, no buy orders - remove it 
          if(!hasBuyOrders && !hasSellOrders && isManaged){
            me.spreadCount -= 1
            om.removeManagedHolding(spread)
            om.saveManagedHoldings()
          }
        }
        var holding = me.hasHolding(getCoinFromPair(spread.pair))
        if(!hasSellOrders && holding){
          me.placeSpreadSellOrder(spread,holding)
        }
      // }else{  //if it's a managed spread
        /** do we have any orders? */
        // if(!hasBuyOrders){
        //   /** place the  order */
        //   me.placeSpreadOrder(spread)
        // }else{

        // }
      }
    // }
    var tmp = me.spreadHoldings

  },
  
  // checkPairIsEligible: (tph)=>{
  //    if(tph.volume24hr > me.minVolume 
  //       && tph.spread > me.minSpread
  //       ){
  //      /** add them to spreadBook */
  //      me.createSpreadPair(tph)
  //    }
  // },
  updateSpreadPairs: ()=>{
    for(spread of me.spreadPairs){
      var tph = globals.tradePairHistories.find(x=>x.pair==spread.pair)
      if(tph){
        spread.updateSpread(tph)
      }
    }
  },
  createSpreadPairs: ()=>{
    /** filter tradePairHistories for only BTC_ pairs */
    var tphs = globals.tradePairHistories.filter(function(item){
      return item.pair.includes("BTC_") 
    })
    /** find and update all pairs that are in our spread pairs */
    for (tph of tphs){
        // var spPair = me.spreadPairs.find(x =>x.pair == tph.pair)
        // if(spPair){
        //   /** update the spread - spread self checks viability*/
        //   spPair.updateSpread(tph)
          
        // }else{
          /** make sure this pair is not already a holding */
          if(!om.isInManagedHoldings(tph.pair)){
             /** add the pair if eligible */
             me.createSpreadPair(tph)
          // }
        }
    }
    /** sort by largest spread and filter for min values */
    me.spreadPairs = me.spreadPairs.sort(function(a,b){
      return b.volume - a.volume
    })

    me.didCreateSpreadPairs = true
  },
  placeSpreadSellOrder: (spread,holding)=>{
    var price = spread.lowAsk()
    var amount = holding[1].available

    polon.exchange_methods.sellOrder(spread.pair,price,amount,0,0,1,function(err,res){
      if(err){
        console.log(err)
      }
    })
  },
  placeSpreadBuyOrder: (spread)=>{
     if(me.spreadCount > 0){return}
    if(spread.isEligible()){

      me.spreadCount += 1 //TODO remove - for testing

      var testExit = true
      var price =  spread.highBid() 
      var amount = me.getAmountFromPrice(price)
      
      spread.buyingStarted = true
      var spData = {
        spread: spread,
        price: price,
        amount: amount,
        total: me.USDtoBTC(me.spreadUSDsize),
      }
      // if(testExit){return}
      //pair, price, amount, fillOrKill, immediateOrCancel, postOnly, callback
      polon.exchange_methods.spread_buy(spread.pair,price,amount,0,0,1,spData,function(err,res,spData){
        if(!err){
          // update the client
          me.getHoldingsFromPolo()

          //put order Number into spData
          spData.orderNumber = res.orderNumber

          //create managed holding - this holds the order ID
          var holding = me.getManagedSpread(spData.spread.pair)
          if(!holding){
            var holding = me.createManagedSpread(spData)
          }else{
            holding.orderNumber = res.orderNumber
          }
          

          //save the managed holdings
          om.saveManagedHoldings()
          
        }else{
          me.spreadCount -=1
          err = err
        }
      })
    }
  },
  adjustSpreadOrder:(oN, rate, amount)=>{
    polon.exchange_methods.moveOrder(oN, rate, amount, 0,1,function(err,res){
      //me.getHoldingsFromPolo()
      res=res
      if(err){
        err=err
      }
    })
  },
  cancelSpreadBuyOrders: (spread)=>{
    for(order of globals.account.orders){
      if(order[0] == spread.pair){
        var data = order[1][0]
        if(data.type == "buy"){
          polon.exchange_methods.cancelOrder(data.orderNumber,function(err,res){
            if(err){
              //if invalid order number, we can stop tying..
              err = err
            }else{
              
            }
          })
        }
      }
    }
  },
  checkAndUpdateOrders: ()=>{
    var refreshOrders = false
    //store the totals of orders for the spread pair
    var orderBuy = 0
    var orderSell = 0
    var spreadsWithBuyOrders = 0
    //loop
    for(var spread of me.spreadPairs){
      if(spread.buyingStarted){
        spreadsWithBuyOrders +=1
      }
      for(order of globals.account.orders){
        if(order[0] == spread.pair){
          if(!spread.buyingStarted){
            spread.buyingStarted = true
            spreadsWithBuyOrders +=1
          }
          var data = order[1][0]
          var rate
          if( data.type=="sell"){
            /** move order bid to top of asks */
            orderSell += pf(data.total)
            rate = spread.lowAsk(data)
            if(pf(data.rate) != rate){
              me.adjustSpreadOrder(data.orderNumber,rate,data.amount)
              refreshOrders = true
            }
          }else{
            /** move order bid to top of bids */
            orderBuy += pf(data.total)
            rate = spread.highBid(data)
            if(pf(data.rate) != rate){
              me.adjustSpreadOrder(data.orderNumber,rate,data.amount)
              refreshOrders = true
            }
          }
        }
      }
      spread.orders = {buy:orderBuy,sell:orderSell} 
    }

    if(refreshOrders){me.getHoldingsFromPolo()}
    
    return spreadsWithBuyOrders
  },
  isManagedSpread: (spread)=>{
    // check if we have a managed holding for this spread
    var holdings = me.managedSpreads()
    for(spreadHolding of holdings){
      if(spreadHolding.pair == spread.pair){
        return true
      }
    }
  },
  hasHolding: (coin)=>{
    for(holding of globals.account.holdings){
      if(holding[0] == coin){
        return holding
      }
    }
  },
  getManagedSpread: (pair)=>{
    return om.isInManagedHoldings(pair,1)
  },
  createManagedSpread: (spData)=>{
    return om.createManagedHolding(spData.spread.pair,new Date(),spData.price,spData.total,spData.orderNumber,2,0.1,true,0,0,0,1,spData.amount)
  },
  managedSpreads: ()=>{
    /** return managed holdings of type "spread'" */
    return om.managedHoldings.filter(item =>item.type == 1)
  },
  createSpreadsFromManagedSpreads: ()=>{
    var managedSpreads = me.managedSpreads()
    for(ms of managedSpreads){
      var tph = globals.tradePairHistories.find(x => x.pair == ms.pair)
      me.createSpreadPair(tph,true)
      me.spreadCount += 1
    }
    
  },
  manageSpread: (spread)=>{
  },
  getSpreadOrders: (spread)=>{
  },
  createSpreadPair: (tph,buyingStarted)=>{
    //creates spread item and adds it to the me.spreadPairs array
    
    var spread = {
      pair:tph.pair,
      created: new Date(),
      buyingStarted: buyingStarted,
      orders: {buy:0,sell:0},
      orderBook: [],
      orderNumber:"",
      volume: 0,
      lowAsk: function(data){
        try {
          if(data && spread.orderBook.asks[0][0] == data.rate &&
            spread.orderBook.asks[0][1] == data.amount){
              var rate = me.pf( (me.pf(spread.orderBook.asks[1][0]) - me.sat(me.spreadSat)) )
              return rate
          }else{
            var rate = me.pf( (me.pf(spread.orderBook.asks[0][0]) - me.sat(me.spreadSat)) )
            return rate
          }  
        }catch(e){ }
      },
      highBid: function(data){
        try{
          if(data && spread.orderBook.bids[0][0] == data.rate && 
            spread.orderBookbids[0][1] == data.amount){
              var rate = me.pf( (me.pf(spread.orderBook.bids[1][0]) + me.sat(me.spreadSat)) )
              return rate
          }else{
            var rate = me.pf( (me.pf(spread.orderBook.bids[0][0]) + me.sat(me.spreadSat)) )
            return rate
          } 
        }catch(e){ }
      },
      spread: 0,
      status: enum_spreadStatus.created,
      spreadArr: [],
      priceArr: [],
      priceDecreasing: false,
      spreadDecreasing: false,
      _isDecreasing: function(arr){
        var sumFirst=0, sumLast=0
        for (i=0; i<5; i++){sumFirst += arr[i]}
        for (i=arr.length-5; i < arr.length; i++){sumLast += arr[i]}
        if(sumLast >= sumFirst){return false}
        else{return true}
      },
      avgSpread: function(){return this.spreadArr.reduce((p, c)=>p+c) / this.spreadArr.length},
      disable: function(){
        this.status = enum_spreadStatus.paused
        this.buyingStarted = false
      },
      enable: function(){
        this.status = enum_spreadStatus.eligible
      },
      isEligible: function(){
        return this.status == enum_spreadStatus.eligible
      },
      updateSpread: function(tph){
        this.spread = tph.spread
        this.spreadArr.push(tph.spread)
        this.priceArr.push(parseFloat(tph.ticker.last))
        this.orderBook = tph.orderBook
        this.volume = tph.volume24hr
        if(this.spreadArr.length > me.spreadAverageArrayLength){
          this.spreadArr.shift()
          this.priceArr.shift()
          if(this.status == enum_spreadStatus.created){
            this.enable()
          }
        }
        /** self check if spread is in viable parameters */
        if(this.status > enum_spreadStatus.created){
          if(this.avgSpread() < me.minSpread){
            this.disable()
          }else{
            this.priceDecreasing = this._isDecreasing(this.priceArr) //only need to check if eligible
            this.spreadDecreasing = this._isDecreasing(this.spreadArr) 
            if(
              (this.spreadDecreasing && this.spread < me.minSpread)
              || this.priceDecreasing || this.spread < me.minSpread
              ){
                this.enable()
              }
            else{
              this.disable()
            }
          }
        }
      }
    }
    spread.updateSpread(tph)
    me.spreadPairs.push(spread)
  },
  sat: (num)=>{
    return num * .00000001
  },
  USDtoBTC: (usd)=>{
    return pf(1 / polon.BTCPrice * usd)
  },
  BTCtoUSD: (btc)=>{
    return pf(polon.BTCPrice * btc)
  },
  
  getAmountFromPrice: (price)=>{
    return functions.fixPrice(me.USDtoBTC(me.spreadUSDsize)/ price)
  },
  getHoldingsFromPolo: (/** Strength:1 to 4 */strength)=>{
    strength = strength || 1

    /** put in a sequence of requests, to hopefully get a fast update */
    for (var i = 0; i <= strength-1; i++) {
       var len = 2000 + (i*1010)
       polon.exchange_methods.getHoldings(len, function(err,res){

       })
       polon.exchange_methods.getOrders(len + 250, function(err,res){

       })
    }
  },
  pf: function(numOrString){return pf(numOrString)}
}

function pf(numOrString){
  return parseFloat( parseFloat(numOrString).toFixed(8) )
}
function getCoinFromPair(pair){
  return pair.split("_")[1]
}
var enum_spreadStatus = {
  created: 0,
  eligible: 1,
  paused: 2
}