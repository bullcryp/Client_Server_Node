SELECT 
    startDate,
    endDate,
    pair,
    -- price,
    JSON_EXTRACT(data, '$.result.t_price.t') AS t_price,
    JSON_EXTRACT(data, '$.result.t_bvol.t') AS t_bvol,
    JSON_EXTRACT(data, '$.result.t_svol.t') AS t_svol,
    JSON_EXTRACT(data, '$.result.t_btrades.t') AS t_btrades,
    JSON_EXTRACT(data, '$.result.t_strades.t') AS t_strades,
    JSON_EXTRACT(data, '$.result.dv_lchunk') AS block_calm_factor,
    JSON_EXTRACT(data, '$.result.slicesAboveBVolMean') AS slicesAboveMean
    -- JSON_EXTRACT(data, '$.result.mfi') AS mfi
FROM
    polodb.backtests
WHERE
    pair LIKE '%%'
-- and JSON_EXTRACT(data, '$.result.slicesAboveBVolMean') > 5
    order by endDate desc
        --  AND JSON_EXTRACT(results, '$.t_bvol.t') > 4 and JSON_EXTRACT(results, '$.t_svol.t') < 1
-- ORDER BY pair, hitdate DESC;-- FROM polodb.backtests;
