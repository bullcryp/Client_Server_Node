select * FROM(SELECT table_name, SUM(TABLE_ROWS) as rows 
FROM information_schema.tables 
WHERE table_schema = 'polodb' -- or your own schema
    AND  table_type ='BASE TABLE'
    AND table_name LIKE 'BTC_%'
GROUP BY table_name WITH ROLLUP ) t
order by rows desc
