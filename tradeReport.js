
functions = require('./functions')
var globals = require('./globals')
var polon = require('./polo_functions')
var db = require("./db/db")
var algo = require('./algo')
var _ = require('lodash')
var moment = require("moment")

// var async = require('async')


var nStats = require("./nodeStats")
// var fs = require('fs');
// var loadFile = (file)=>{eval(fs.readFileSync(file)+'')}
// loadFile('./nodeStats.js')

var block_watch = 6; // last 6 slices to watch
var block_size = 24;
var sliceCount = 480
var pairsArr=[]
var pairIndex=0
var pairsCount=0
// var aq = async.queue(function (task, callback) {
//     testPairs(task.pair)
//     callback()
// },1)
// aq.drain = function(){
//     console.log("All Pairsets Done")
// }

var me = module.exports = {
    /**  DEBUG send data for pump analysis */
    
     processLiveChunk: function(tph, callback){
         var result;
         
        //merge currentTrades into an aggregate item
        // var aggregateLiveTrades = makeAggregateTradeObject(tph.trades)
        var pair = tph.pair
        var data = tph.aggregateTrades.slice(tph.aggregateTrades.length-480)
        var dataCurrent = tph.dataCurrent

        /** filter trades data to 4 hour time slice max */
        var endDate = data[0].dateStart
        var startDate = moment(data[0].dateStart).subtract(4,"hours").toDate()
        var filtered_Trades = data.filter(x=>x.dateStart >= startDate)
        var prepared_trades = []
        //var price = parseFloat(data[0].rate)

        // aggregateTrades.unshift(aggregateLiveTrades)
        /** reverse trades for proper processing in processLChunk */
        filtered_Trades.reverse()

        for(var item of filtered_Trades) {
          prepared_trades.push(makeAggregate(item))
        }

        /** this writes the data to 1LIVERESULTS, and
         * if it returns a result, would be our 'buy' signal.
         */
        result = processLChunk(prepared_trades)

        //and send a result
        if(result){
            /** this would return the 'buy' signal */
            callback(result, dataCurrent)
        }else{
            callback(undefined)
        }
        /** make each slice into format readable by processLChunk */
        function makeAggregate(item) {
            return {
                startDate:item.dateStart,
                endDate: item.dateEnd,
                startTradeId:item.startTradeId,
                endTradeId:item.endTradeId,
                high: item.high,
                low: item.low,
                close: item.close,
                open: null,
                sell_amount:item.sell.amount,
                sell_price:item.sell.price,
                sell_total:item.sell.total,
                sell_trades:item.sell.trades,
                buy_amount:item.buy.amount,
                buy_price:item.buy.price,
                buy_total:item.buy.total,
                buy_trades:item.buy.trades,
            }
        }
        function processLChunk(trades){
    
            /**t_calc results - compare left chunk to right chunk */
            var pricedata =  t_calc(trades,"close") //run data thru t_calc, get t_calc_data
            var buyvolumedata = t_calc(trades,"buy_amount")
            var sellvolumedata = t_calc(trades,"sell_amount")
            // var buysellvolumedata = t_calc(chunk,"")
            var buytradesdata = t_calc(trades,"buy_trades")
            var selltradesdata = t_calc(trades,"sell_trades")
            
                var lchunk = trades.slice(0,trades.length - block_watch)
                var rchunk = trades.slice(trades.length - (block_watch+4))

                var lChunkPrice = _.map(lchunk, "close").map(Number);
                var lChunkBVol = _.map(lchunk,"buy_amount").map(Number)
                var rChunkBVol = _.map(rchunk,"buy_amount").map(Number)

            /** determine the number of block_watch slices that have bvol above the lchunk mean */
                var lChunkBVolMean = nStats.mean(lChunkBVol)
                var a = ""
                for(var item of rChunkBVol){
                    if (item >= lChunkBVolMean){
                        //TODO - calculate how many std-dev above mean the item is,
                        // and use that value in place of 1
                        a += "1"
                    }else{
                        a += "0"
                    }
                }
                var slicesAboveBVolMean = parseInt(a,2)
                
            /** lchunk price deviation - calm factor*/
                var lBlockMean = nStats.mean(lChunkPrice)
                var lBlockStDev = nStats.standard_deviation(lChunkPrice)
            var lChunkDev = lBlockStDev/lBlockMean
            
            /** criteria to save to DB */
            var Data = {}
            Data.result ={
                        startDate: trades[0].startDate,
                        endDate: trades[trades.length-1].endDate,
                        price: trades[trades.length-1].close,
                        t_price: pricedata,
                        t_bvol: buyvolumedata,
                        t_svol: sellvolumedata,
                        t_btrades: buytradesdata,
                        t_strades: selltradesdata,
                        dv_lchunk: lChunkDev,
                        slicesAboveBVolMean: slicesAboveBVolMean
                    }
            
            /** store result in DB for backtest tweaking */
            if(
                algo.validateSlices(slicesAboveBVolMean,3)  ||
                Math.abs(pricedata.t) >= 2 ||
                buyvolumedata.t >= 3 ||
                sellvolumedata.t >= 3 ||
                buytradesdata.t >= 3 ||
                selltradesdata.t >= 3 
              )
                // algo.validateSlices(slicesAboveBVolMean,4) )
                {
                    db.updateBacktestData(pair,[Data],function(){
                        // no action needed on callback
                    })
            }
            if(
                algo.validateSlices(slicesAboveBVolMean,3)  ||
                Math.abs(pricedata.t) >= 1.5 
                //|| buyvolumedata.t >= 2 ||
                // sellvolumedata.t >= 2 ||
                // buytradesdata.t >= 2 ||
                // selltradesdata.t >= 2 
              ){
                sendAnalysisToClient(Data,pair)
              }
            
            return {result:Data.result, pair:pair}
            // }else{
            //     return undefined
            // }
            
            // return undefined
        }
     }
                
}  /** end module.exports */     

function sendAnalysisToClient(Data,pair){
    
        var n= Data.result.slicesAboveBVolMean.toString(2);
        n="0000000000".substr(n.length)+n;
        var log =  n + "  " + pair + 
            "  pr:" + Data.result.price + 
            "  pT:" + Data.result.t_price.t.toFixed(2) + 
            "  bT:" + Data.result.t_bvol.t.toFixed(2) + 
            "  sT:" + Data.result.t_svol.t.toFixed(2) + 
            "  btT:" + Data.result.t_btrades.t.toFixed(3) +
            "  sTT:" + Data.result.t_strades.t.toFixed(3) +
            "  clm:" + Data.result.dv_lchunk.toFixed(6)  + 
            " -- " + functions.sqlDate(Data.result.endDate)
        console.log(log)
    
        try{
          globals.gsocket.emit('console_log', log)
        }catch(e){}
        // try {
        //     icClient.say('#bitcoin',log)
        // }catch(e){  }

    
}
// var ircClient = require('node-irc')
// var icClient = new ircClient('52.43.210.237', 6667, 'Kryptonite', 'KryptoniteTrader');
// icClient.debug = true

// icClient.on('ready', function () {
//   icClient.join('#bitcoin');
//   icClient.say('#bitcoin', 'Kryptonite Trader is here!!!.');

// });
// icClient.on('error', function(e){
//   console.log(e)
// })
// icClient.connect()


 
function t_calc(myData, mySeg, n_slices) { // data contains "close" price
    
    mySeg = mySeg || "close"; // use closing price
	n_slices = n_slices || block_watch; // block_watch == 12 slices, 6 minutes

    if (myData[0][mySeg]==undefined || myData.length < 2 * n_slices) { // at least two equal samples, else t-test is meaningless
        return { "t": 0, "dof": 0, "std_err": 0, "p_value": 0.5 }
    }
 
    // myData.reverse()
    var close_arr = _.map(myData, mySeg).map(Number); // array of closing prices
    var close_final = close_arr.splice(close_arr.length - n_slices, close_arr.length); // filter out final block
    var output = nStats.t_test2sample(close_final, close_arr)

    return output;
}

// http://stockcharts.com/school/doku.php?id=chart_school:technical_indicators:money_flow_index_mfi
function mfi_calc(myData) { // mfi of  last 'block_watch + 1' slices.
    var up_or_down, prev_price, plus_flow = [], minus_flow = [];
    if (myData.length < 2 * block_watch) {
        return 0;
    } else if (myData.length < (2 * block_watch + 1)) {
        var tmp_idx = myData.length - 2 * block_watch - 1;
    } else {
        var tmp_idx = myData.length - 2 * block_watch;
    }
    for (var i = tmp_idx; i < myData.length; i++) {
        if (prev_price == undefined) {
            // console.log(i, myData.length)
            prev_price = myData[i].close;
        } else {
            up_or_down = (myData[i].close - prev_price);
            prev_price = myData[i].close;
            var typical_price = (myData[i].high + myData[i].low + myData[i].close) / 3
            var raw_money_flow = typical_price * (myData[i].buy_amount + myData[i].sell_amount);
            if (up_or_down > 0) { // positive money flow
                plus_flow.push(raw_money_flow);
            } else { // negative money flow
                minus_flow.push(raw_money_flow);
            }
        }
    }

    var money_flow_ratio = nStats.sum(plus_flow) / nStats.sum(minus_flow);
    var money_flow_index = 100 - 100 / (1 + money_flow_ratio);
    //console.log(money_flow_ratio, plus_flow, minus_flow)
    return money_flow_index;
}

function hide_Comments_I_Want_To_Save_Cause_I_Can_Fold_This_In_IDE_LOL(){   

        //  runBackTestData:  function (pair, startDate, endDate) {

        //     /** get data from each coin, and feed it in chunks to the detection
        //      * algorithm
        //      */

        //     if (!pair) { 
        //         pairsArr = globals.pairSets.slice()
        //      }else{
        //          pairsArr = [pair]
        //      }
        //      pairsCount = pairsArr.length

        //     testPairs(pairIndex)

        //  },  
        // function testPairs(pairIndex){
        //     var pair = pairsArr[pairIndex]
            
        //     db.getTradePairForBackTest(pair, function(data){
                
        //         if(data.length < 1){
        //             advancePair()
        //             return
        //         }

        //         processChunks(pair, data, function(){
        //             advancePair()
        //         })
                
        //     })
            
        // }

        // function advancePair(){
        //     pairIndex += 1
        //     if (pairIndex <= pairsCount){
        //         testPairs(pairIndex)
        //     }else{
        //         console.log("End of Pairs")
        //     }
        // }

        // function processChunks(pair, data, callback){

        //     var t_calc_array = [], chunk, chunkResult
        //         var start = 0
        //         var end = sliceCount
        //         var slices = data.length-1
        //         var chunks=1
        //         functions.jettyClear()
        //         do {
        //             functions.jettyLog("testing: " + pair + " " + functions.getPercentRemaining(end, slices) + "%",20)

        //             //slice the chunk from the data array
        //             chunk = data.slice(start,end)
        //             var endDate = chunk[chunk.length-1].endDate
        //             startDate = moment(endDate).subtract(4,"hours").toDate()
        //             // var aggTrades = tph.aggregateTrades.filter(x=>x.dateStart >= startDate)

        //             var fchunk = chunk.filter(x=>x.startDate >= startDate)
        //             var tmp = {
        //                 pair: pair,
        //                 result: processChunk(fchunk)
        //             }
        //             if(tmp.result){
        //                 t_calc_array.push( tmp ) //this method gets the t_calc results, and writes a flag to the DB if pump detected
        //             }
        //             start += 1; end +=1; chunks+= 1  //advance chunk to the next segment
                    
        //         } while (end <= slices);

        //         console.log("processed " + chunks + " chunks")

        //         if (t_calc_array.length > 0){
        //             db.updateBacktestData(pair, t_calc_array, function(){
        //                 callback()
        //             })
        //         }else{
        //             callback()
        //         }
                
        // }

        // function processChunk(block){
            
        //     /**t_calc results - compare left chunk to right chunk */
        //     var pricedata =  t_calc(block,"close") //run data thru t_calc, get t_calc_data
        //     var buyvolumedata = t_calc(block,"buy_amount")
        //     var sellvolumedata = t_calc(block,"sell_amount")
        //     // var buysellvolumedata = t_calc(chunk,"")
        //     var buytradesdata = t_calc(block,"buy_trades")
        //     var selltradesdata = t_calc(block,"sell_trades")
            
        //         var lchunk = block.slice(0,block.length - block_watch)
        //         var rchunk = block.slice(block.length - block_watch)

        //         var lChunkPrice = _.map(lchunk, "close").map(Number);
        //         var lChunkBVol = _.map(lchunk,"buy_amount").map(Number)
        //         var rChunkBVol = _.map(rchunk,"buy_amount").map(Number)

        //     /** determine the number of block_watch slices that have bvol above the lchunk mean */
        //         var lChunkBVolMean = nStats.mean(lChunkBVol)
        //     var slicesAboveBVolMean = 0
        //         rChunkBVol.forEach(function(item){
        //             if (item > lChunkBVolMean)
        //                 slicesAboveBVolMean += 1
        //         })
                
        //     /** lchunk price deviation - calm factor*/
        //         var lBlockMean = nStats.mean(lChunkPrice)
        //         var lBlockStDev = nStats.standard_deviation(lChunkPrice)
        //     var lChunkDev = lBlockStDev/lBlockMean

        //     if(Math.abs(pricedata.t) >= 4 ||
        //         buyvolumedata.t >= 3 ||
        //         sellvolumedata.t >= 3 ||
        //         buytradesdata.t >= 3 ||
        //         selltradesdata.t >= 3){
        //         return {
        //             startDate: block[0].startDate,
        //             endDate: block[block.length-1].endDate,
        //             price: block[block.length-1].close,
        //             t_price: pricedata,
        //             t_bvol: buyvolumedata,
        //             t_svol: sellvolumedata,
        //             t_btrades: buytradesdata,
        //             t_strades: selltradesdata,
        //             dv_lchunk: lChunkDev,
        //             slicesAboveBVolMean: slicesAboveBVolMean
        //             // mfi: mfidata
        //         }
        //     }else{
        //         return undefined
        //     }
            
        // }         
        //         //TODO remove debugging
        //         var obj = {pair: pr,data: t_calc(data)}
        //         db.updateBacktestData(globals.debugUserName,pr,obj,function(){})

        // var t_calc_data =  t_calc(data) //run data thru t_calc, get t_calc_data
        // if(t_calc_data.t > 5){
        //     t_log.push(t_calc_data)
        //     if (!globals.pairTestDidOutPutFIle){
        //         globals.pairTestDidOutPutFIle = true
        //         fs.writeFile('./tmp/data.json', JSON.stringify(data),function(err){
        //         });
                
        //     }
        // }

        //         start -= 1  

        //         if(start % 20==0){console.log(start)}

        //         if (start < count){
        //             //that's all folks.
        //             var obj = {pair: pr,data: t_log}
        //             db.updateBacktestData(globals.debugUserName,pr,obj,function(){

        //             })
        //             return t_log
        //         }else{
        //             //getChunk(pr,start,count,t_log)
        //         }

        // }



        //getChunk(pair,start,count,t_log,pairCount)



        // do {

        //     db.getTradePairSlices(pair,start,count,t_log,function(err,data){
        //         //data should now be a 480 slice chunk
        //         if(err){
        //             console.log(err)
        //         }
        //         var myStart = start
        //         t_log.push( t_calc(data) )
        //         if(myStart == 0){
        //             //we're done
        //             console.log(t_log)
        //         }
        //     })
        //     start = Math.max(start-count,0)
        //     console.log(start)
        // } while (start > 0);
        // // console.log(t_log)


        // db.getTradePairForBackTest(pair, currPair, function (slices, currePairNum) {

        //     var newline = String.fromCharCode(13, 10);

        //     var position
        //     do {
        //         position = slices.length - 480
        //         // console.log(slices.length)
        //         var chunk = slices.splice(position)
        //         // console.log(slices.length)
        //         // console.log(chunk[chunk.length-1].dateStart)
        //         // console.log(JSON.stringify(chunk))
        //         //db.updateBacktestData("Stef",JSON.stringify(chunk),function(){

        //     // })

        //         console.log(t)
        //         // m_log += m = mfi_calc(chunk) + newline
        //         // console.log(t)
        //         // console.log(m)
        //         position = position - 1
        //     } while (position >= 0)
        //     t_log = "T_Calc for Pair: " + pair + newline + t_log + newline + newline
        //     m_log = "MFI_Calc for Pair: " + pair + newline + m_log
        //     // me.runBackTestAllData(currPairNum + 1)
        //     db.updateBacktestData(defaultUsername,t_log + m_log,function(){

        //     })

        // })

        // })

        //     statistical functions 
        //     isArray: (obj) => {
        //         return Object.prototype.toString.call(obj) === "[object Array]";
        //     },
        //     getNumWithSetDec: (num, numOfDec) => {
        //         var pow10s = Math.pow(10, numOfDec || 0);
        //         return (numOfDec) ? Math.round(pow10s * num) / pow10s : num;
        //     },
        //     getAverageFromNumArr: (numArr, numOfDec) => {
        //         if (!me.isArray(numArr)) { return false; }
        //         var i = numArr.length,
        //             sum = 0;
        //         while (i--) {
        //             sum += numArr[i];
        //         }
        //         return me.getNumWithSetDec((sum / numArr.length), numOfDec);
        //     },
        //     getVariance: (numArr, numOfDec) => {
        //         if (!me.isArray(numArr)) { return false; }
        //         var avg = me.getAverageFromNumArr(numArr, numOfDec),
        //             i = numArr.length,
        //             v = 0;

        //         while (i--) {
        //             v += Math.pow((numArr[i] - avg), 2);
        //         }
        //         v /= numArr.length;
        //         return me.getNumWithSetDec(v, numOfDec);
        //     },
        //     getStandardDeviation: (numArr, numOfDec) => {
        //         if (!me.isArray(numArr)) { return false; }
        //         var stdDev = Math.sqrt(me.getVariance(numArr, numOfDec));
        //         return me.getNumWithSetDec(stdDev, numOfDec);
        //     },
        //     sumArray: (arr) => {
        //         function getSum(total, num) {
        //             return total + num;
        //         }
        //         return arr.reduce(sum)
        //     },
        //     getObjectArraySegmentAsArray: (array, objectPart, filterZero) => {
        //         var arr = _.map(array, _.property(objectPart))
        //         if (filterZero) { arr = arr.filter(function (i) { return i }) }
        //         return arr
        //     },
        //     processPairSlicesChunk: (pair, data) => {

        //         //get the data length
        //         var aLen = data.length
        //         //separate the last n slices to new array
        //         var main_data = data.splice(4)
        //         var head_data = data

        //         // console.log(pair + " - " + JSON.stringify(head_data))

        //         var hbto = me.getObjectArraySegmentAsArray(head_data, 'sell.total', false)
        //         var hbtr = me.getObjectArraySegmentAsArray(head_data, 'sell.trades', false)
        //         var bto = me.getObjectArraySegmentAsArray(data, 'buy.total', false)
        //         var btr = me.getObjectArraySegmentAsArray(data, 'buy.trades', false)
        //         me.processArray([sto, str], "Sell total, trades")
        //         me.processArray([bto, btr], "Buy total, trades")
        //     },
        //     processArray: (arr, title) => {
        //         console.log(title)
        //         arr.forEach(function (e, i) {
        //             console.log("Average: " + me.getAverageFromNumArr(e, 8)
        //                 + " | Variance: " + me.getVariance(e, 8)
        //                 + " | stDv: " + me.getStandardDeviation(e, 8)
        //             )
        //         })

        //     },

}


