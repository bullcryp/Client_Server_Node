
var globals = require('./globals')
var functions = require('./functions')
var moment = require('moment')

var me = module.exports = {
  getTradePairHistory: (pair, create)=>{
    /** get tradePairHistory, or create one if not exist */
    var tph = globals.tradePairHistories.find(x => x.pair == pair)
    if(tph == undefined && create){
        globals.tradePairHistories.push({
          pair: pair,
          lastTradeTimeStampStart: moment().subtract(globals.marketTradeHistoryLength, "days").toDate(),
          lastTradeId: 0,
          poloTrades: [],
          volume24hr: 0,
          tradesSum: [], //keep money flow stats 
          dataCurrent: false,
          didDataRecovery: false
        })
        tph = globals.tradePairHistories.find(x => x.pair == pair)
    }

    return tph
  },
  receiveTrades: (tradeObj)=>{
    tradeObj = JSON.parse(tradeObj)
    var tradeEl 
    functions.log(tradeObj.exchange + " : " + tradeObj.pair + " : trades: " + (tradeObj.trades.length || 1) )
    var pair = tradeObj.pair
    var tph = me.getTradePairHistory(pair,true)

    if(tradeObj.type == "agg_current"){
        tradeEl = functions.mergeArrays(tradeObj.trades, tph.poloTrades)  
        tradeEl.length = globals.inMemoryAnalysisChunkSize
    }else{
        tradeEl = tradeObj.trades
    }

    switch (tradeObj.exchange) {
        case "Polo":
            tph.poloTrades = tradeEl
            break;
    
        case "Bittrex":
            tph.bittrexTrades = tradeEl
            break;
        default:
            break;
    }
    
}

}